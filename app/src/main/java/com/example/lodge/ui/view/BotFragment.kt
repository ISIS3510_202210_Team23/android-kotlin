package com.example.lodge.ui.view

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.lodge.R
import com.example.lodge.core.bot.BotResponse
import com.example.lodge.data.model.bot.Message
import com.example.lodge.databinding.FragmentBotBinding
import com.example.lodge.ui.view.adapters.MessagingAdapter
import com.example.lodge.ui.viewmodel.BotViewModel
import com.example.lodge.utils.NetworkHelper
import com.ibm.cloud.sdk.core.security.IamAuthenticator
import com.ibm.watson.assistant.v2.Assistant
import com.ibm.watson.assistant.v2.model.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.collect


const val USUARIO = 0
const val BOT = 1
const val ENTRADA_DE_VOZ = 2

@AndroidEntryPoint
class BotFragment: Fragment(R.layout.fragment_bot) {

    private val viewModel: BotViewModel by activityViewModels()
    private var _binding: FragmentBotBinding? = null
    private val binding get() = _binding

    private val TAG = "MainActivity"
    //private var watsonAssistant: Assistant? = null
    //private var watsonAssistantSession: Response<SessionResponse>? = null

    //You can ignore this messageList if you're coming from the tutorial,
    // it was used only for my personal debugging
    var messagesList = mutableListOf<Message>()

    private lateinit var adapter: MessagingAdapter
    private val botList = listOf("Peter", "Francesca", "Luigi", "Igor")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        _binding = FragmentBotBinding.inflate(inflater, container, false)
        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)

        if (viewTopBar != null) {
            viewTopBar.visibility = View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility = View.GONE
        }

        val scrollview = activity?.findViewById<ScrollView>(R.id.scroll_chat)
        if (scrollview != null) {
            scrollview.post {
                scrollview.fullScroll(ScrollView.FOCUS_DOWN)
            }
        }

        val cajaMensajes = binding?.cajadetexto
        if (cajaMensajes != null) {
            cajaMensajes.setOnKeyListener { view, keyCode, event ->
                if (event.action === KeyEvent.ACTION_DOWN) {
                    when (keyCode) {
                        KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                            binding?.enviar
                                ?.let {
                                activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                                    enviarMensaje(it)
                                }
                                }
                                }
                            true
                        }
                        else -> {
                        }
                    }
                }
                false
            }
        }

        binding?.enviar?.setOnClickListener{
            activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                enviarMensaje(it)
            }
            }
            }




        return binding?.root
    }

    private fun enviarMensaje(view: View){
        println("ENTROOOO")
        // Obtenemos el mensaje de la caja de texto y lo pasamos a String
        val mensaje = binding?.cajadetexto?.text.toString()

        // Si el usuario no ha escrito un mensaje en la caja de texto y presiona el botón enviar, le mostramos
        // un Toast con un mensaje 'Ingresa tu mensaje ...'
        if (mensaje.trim { it <= ' ' }.isEmpty()) {
            Toast.makeText(context, "Say hi", Toast.LENGTH_LONG).show()
        }

        // Si el usuario agrego un mensaje a la caja de texto, llamamos al método agregarTexto()
        else {
            agregarTexto(mensaje, USUARIO)
            binding?.cajadetexto?.setText("")
            viewModel.createMessageBot(mensaje)

            // Enviamos la consulta del usuario al Bot
            validar(mensaje)
        }
    }


    fun validar(response: String) {

        var respuestaBot: String = ""

        respuestaBot = BotResponse.basicResponses(response)
        agregarTexto(respuestaBot, BOT)
        /*withContext(IO) {


            /*var watsonAssistant =
                Assistant(
                    "2021-06-14",
                    IamAuthenticator("gXG8ZDRouAMf82Bf3TtJ6EP1TDBvCWduZcUHX_6szr2U")
                )
            watsonAssistant!!.serviceUrl =
                "https://api.us-east.assistant.watson.cloud.ibm.com/instances/eca556eb-71ab-416b-a738-a0481f23a9a7"

                println("BOT: "+watsonAssistant)
                *//*val call = CreateSessionOptions.Builder()
                        .assistantId("3c73c201-9136-46b9-9aab-c04de2c01e6f").build()

                val responseSession: SessionResponse =
                    watsonAssistant!!.createSession(call).execute().getResult()*//*

            val call2 = watsonAssistant.createSession(
                CreateSessionOptions.Builder()
                    .assistantId("3c73c201-9136-46b9-9aab-c04de2c01e6f").build()
            )
           val watsonAssistantSession = call2.execute()

                println(watsonAssistantSession.result)
                val input: MessageInput = MessageInput.Builder()
                    .messageType("text")
                    .text(response)
                    .build()

                val options: MessageOptions = MessageOptions.Builder().assistantId("3c73c201-9136-46b9-9aab-c04de2c01e6f").sessionId(watsonAssistantSession.getResult().getSessionId())
                    .input(input)
                    .build()

                val responseBot: MessageResponse = watsonAssistant!!.message(options).execute().result
                println(responseBot)
                respuestaBot = responseBot.output.generic[0].text()
                agregarTexto(respuestaBot, BOT)*/








        }*/
    }

    private fun agregarTexto(mensaje: String, type: Int) {

        // Coloco el FramLayout dentro de la variable layoutFrm
        val layoutFrm: FrameLayout



        // Según sea el rol, le cargamos el FrameLayout y llamamos a un método respectivo
        // Los métodos agregarTextoUsuario() y agregarTextoBot() los crearé más adelante
        when (type) {
            USUARIO -> layoutFrm = agregarTextoUsuario()
            BOT -> layoutFrm = agregarTextoBot()
            else -> layoutFrm = agregarTextoBot()
        }

        // Si el usuario hace clic en la caja de texto
        layoutFrm.isFocusableInTouchMode = true

        // Pasamos un LinearLayout
        activity?.findViewById<LinearLayout>(R.id.linear_chat)?.addView(layoutFrm)

        // Mostramos los textos de los mensajes en un TextView
        val textview = layoutFrm.findViewById<TextView>(R.id.msg_chat)
        textview.setText(mensaje)


        // Si el usuario sale del modo escritura, ocultamos el teclado del dispositivo
        // El método 'ocultarTeclado()' lo crearemos más adelante
        activity?.let { configTeclado.ocultarTeclado(it) }

        // Enfocamos el TextView Automáticamente
        layoutFrm.requestFocus()

        // Volvemos a cambiar el enfoque para editar el texto y continuar escribiendo
        binding?.cajadetexto?.requestFocus()

        // Si es un cliente el que envía un mensaje al Bot, cargamos el método 'TexToSpeech'
        // 'TexToSpeech' junto a otras métodos procesa los mensajes de voz que seran enviados al Bot


    }

    fun agregarTextoUsuario(): FrameLayout {
        val inflater = LayoutInflater.from(context)
        return inflater.inflate(R.layout.mensaje_usuario, null) as FrameLayout
    }

    // Colocamos los mensajes del Bot en el layout 'mensaje_bot'
    fun agregarTextoBot(): FrameLayout {
        val inflater = LayoutInflater.from(context)
        return inflater.inflate(R.layout.mensaje_bot, null) as FrameLayout
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.allEventsFlow.collect { event ->
                when(event){
                    is BotViewModel.AllEvents.Error -> {
                        binding?.apply {
                            Toast.makeText(requireContext(), event.error, Toast.LENGTH_SHORT).show()
                        }
                    }
                    is BotViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }


                    else ->{
                        Log.d(TAG, "listenToChannels: No event received so far")
                    }
                }

            }
        }
    }





    object configTeclado {

        // Mediante esta función ocultamos el teclado cuando no se use en la interface de la aplicación
        fun ocultarTeclado(context: Activity, view: View) {

            // Dentro de la variable 'metodoentrada' hacemos uso de getSystemService y le pasamos el método
            // 'InputMethodManager' el cual detecta el método de entrada que es el teclado
            val metodoentrada = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            // A la variable 'metodoentrada' le pasamos el método 'hideSoftInputFromWindow' que solicita al sistema
            // ocultar el método de entrada de la aplicación, el cual es el teclado y le obtenemos un token con
            // el método 'getWindowToken', por último le pasamos el método 'InputMethodManager' con
            // la función 'HIDE_NOT_ALWAYS' que mantiene siempre en estado oculto el teclado
            metodoentrada.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS)
        }

        // Llamamos a la función 'ocultarTeclado' mediante un 'try'
        fun ocultarTeclado(context: Activity) {
            try {
                ocultarTeclado(context, context.currentFocus!!)
            }

            // Si hay un error lanzamos una excepción mediante 'catch'
            catch (e: Exception) { }
        }

    }


}