package com.example.lodge.utils

import android.os.Build
import android.transition.Slide
import android.transition.TransitionManager
import android.view.*
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.example.lodge.R


object PopupHelper {

    fun showNoInternetPopUp(view: View, inflater: LayoutInflater) {

        // inflate the layout of the popup window
        val popupView: View? = inflater.inflate(R.layout.popup_no_internet_connection, null)

        // create the popup window
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)

        // Set an elevation for the popup window
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }*/

        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.BOTTOM
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            popupWindow.exitTransition = slideOut

        }

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        TransitionManager.beginDelayedTransition(view as ViewGroup?)
        view.post{
            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
        }

    }
}