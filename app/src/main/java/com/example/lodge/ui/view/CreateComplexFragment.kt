package com.example.lodge.ui.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.lodge.R
import com.example.lodge.databinding.ActivityRegisterComplexBinding
import com.example.lodge.ui.viewmodel.ComplexRegisterViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class CreateComplexFragment : Fragment(R.layout.activity_register_complex) {

    private val viewModel : ComplexRegisterViewModel by activityViewModels()
    private val regViewModel: RegisterViewModel by activityViewModels()
    private var _binding : ActivityRegisterComplexBinding? = null
    private val binding get()  = _binding
    private val TAG = "ComplexCodeFragment"
    private var currUser: String =""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityRegisterComplexBinding.inflate(inflater , container , false)

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.GONE
        }

        getCurrentUser()
        registerObservers()
        listenToChannels()
        binding?.apply {
            createComplexBtn.setOnClickListener {
                activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                    val name = nameInput.text.toString()
                    val city = cityInput.text.toString()
                    val address = addressInput.text.toString()


                    regViewModel.createComplex(name,city,address,currUser)
                }
                }



            }

        }

        return binding?.root
    }
    private fun getCurrentUser() {
        regViewModel.getCurrentUser()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun registerObservers() {
        viewModel.currentComplex.observe(viewLifecycleOwner) { user ->
            user?.let {
                findNavController().navigate(R.id.action_createComplexFragment_to_homeFragment)
            }
        }
        regViewModel._firebaseUser.observe(viewLifecycleOwner){ user ->
            user?.let {
                findNavController().navigate(R.id.action_createComplexFragment_to_homeFragment)
            }
        }
    }

    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            regViewModel.allEventsFlowComplex.collect { event ->
                when(event){
                    is RegisterViewModel.AllEvents.Error -> {
                        binding?.apply {
                            errText.text = event.error
                        }
                    }
                    is RegisterViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                    is RegisterViewModel.AllEvents.ErrorCode -> {
                        if (event.code == 1)
                            binding?.apply {
                                nameInput.error = "name should not be empty"
                            }


                        if(event.code == 2)
                            binding?.apply {
                                cityInput.error = "city should not be empty"
                            }

                        if(event.code == 3)
                            binding?.apply {
                                addressInput.error = "address should not be empty"
                            }
                        if(event.code == 4)
                            binding?.apply {
                                nameInput.error = "name should have at least 6 characters"
                            }

                        if(event.code == 5)
                            binding?.apply {
                                nameInput.error = "name should have less than 30 characters"
                            }
                        if(event.code == 6)
                            binding?.apply {
                                cityInput.error = "city should have at least 3 characters"
                            }

                        if(event.code == 7)
                            binding?.apply {
                                cityInput.error = "city should have less than 30 characters"
                            }
                        if(event.code == 8)
                            binding?.apply {
                                addressInput.error = "address should have at least 6 characters"
                            }

                        if(event.code == 9)
                            binding?.apply {
                                addressInput.error = "address should have less than 30 characters"
                            }
                    }

                    else ->{
                        Log.d(TAG, "listenToChannels: No event received so far")
                    }
                }

            }
        }
    }




}