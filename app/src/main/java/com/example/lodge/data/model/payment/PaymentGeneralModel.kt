package com.example.lodge.data.model.payment

import com.google.gson.annotations.SerializedName

/**
 * Data model for the general information shown in the payment screens
 */
data class PaymentGeneralModel(
    @SerializedName("balance") val balance:Double,
    @SerializedName("closingDate") val closingDate: String,
    @SerializedName("dueDate") val dueDate:String)
