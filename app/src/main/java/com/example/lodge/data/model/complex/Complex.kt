package com.example.lodge.data.model.complex

data class Complex(val name:String, val city:String, val address:String, val array: List<String>)
