package com.example.lodge.data.model.user

data class AdminModel(val id:String="",
                      val email:String="",
                      val house:String="",
                      val name:String="",
                      val phone:String="",)
