package com.example.lodge.domain.payment

import com.example.lodge.data.cache.BillsReceiptsMemCache
import com.example.lodge.data.repository.PaymentRepository
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import com.example.lodge.domain.payment.model.ReceiptView
import com.example.lodge.domain.payment.model.toView
import javax.inject.Inject

class GetPaymentReceiptHistoryUseCase @Inject constructor(
    private val repository: PaymentRepository,
    private val userRepository: UserRepository,
    private val memCache: BillsReceiptsMemCache
) {
    suspend operator fun invoke(userId:String, conn:Boolean, next:Boolean=false): List<ReceiptView> {
        val receiptsMemCache = memCache.receiptsMemCache
        val pagedReceipts = memCache.completePagedReceipts
        val result: ArrayList<ReceiptView> = ArrayList()
        val user: UserModel =userRepository.getUserById(userId = userId)

        if(conn){
            val idLast:String? = if (pagedReceipts.size>0) pagedReceipts[pagedReceipts.size-1].id else null
            val offset:String? = if (next) idLast else null

            val net = repository.getReceiptsNetwork(userId=user.id, houseId = user.house, idOffset = offset)
            if(offset==null){
                pagedReceipts.clear()
                receiptsMemCache.clear()
                repository.clearReceipts()
                repository.insertReceipts(net)
                net.map {
                    receiptsMemCache.add(it.toView())
                }
                net.map {
                    pagedReceipts.add(it.toView())
                }
            } else{
                net.map {
                    pagedReceipts.add(it.toView())
                }
            }
            return pagedReceipts
        }
        else{
            pagedReceipts.clear()
            if(receiptsMemCache.size!=0){
                return receiptsMemCache
            }
            else{
                repository.getReceipts(userId=user.id, houseId = user.house)
                    .map {
                        result.add(it.toView())
                    }
            }
            return result
        }
    }
}