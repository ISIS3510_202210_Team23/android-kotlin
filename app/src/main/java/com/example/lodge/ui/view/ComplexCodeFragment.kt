package com.example.lodge.ui.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.lodge.R
import com.example.lodge.databinding.ActivityComplexCodeBinding
import com.example.lodge.databinding.ActivityRegisterComplexBinding
import com.example.lodge.ui.viewmodel.ComplexCodeAssociationViewModel
import com.example.lodge.ui.viewmodel.ComplexRegisterViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ComplexCodeFragment: Fragment(R.layout.activity_complex_code) {

    private val viewModel : ComplexCodeAssociationViewModel by activityViewModels()
    private val regViewModel: RegisterViewModel by activityViewModels()
    private var _binding : ActivityComplexCodeBinding? = null
    private val binding get()  = _binding
    private val TAG = "ComplexCodeFragment"
    private var currUser: String =""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = ActivityComplexCodeBinding.inflate(inflater , container , false)

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.GONE
        }

        getCurrentUser()
        registerObservers()
        listenToChannels()
        binding?.apply {
            button1.setOnClickListener {



                activity?.let { it1 ->
                    if (NetworkHelper.checkForInternet(it1, view, inflater)) {
                        val code = codeInput.text.toString()
                        if(code.isEmpty()){
                            binding.apply {

                            }
                        }
                        regViewModel.associateComplex(code, currUser)
                    }
                }


            }

        }

        return binding?.root
    }
    private fun getCurrentUser() {
        regViewModel.getCurrentUser()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun registerObservers() {
        viewModel.getCurrentCode.observe(viewLifecycleOwner) { user ->
            user?.let {
                findNavController().navigate(R.id.action_complexCodeFragment_to_homeFragment)
            }
        }
        regViewModel._firebaseUser.observe(viewLifecycleOwner){ user ->
            user?.let {
                findNavController().navigate(R.id.action_complexCodeFragment_to_homeFragment)
            }
        }
    }

    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            regViewModel.allEventsFlowCode.collect { event ->
                when(event){
                    is RegisterViewModel.AllEvents.Error -> {
                        binding?.apply {
                            errorText.text = event.error
                        }
                    }
                    is RegisterViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                    is RegisterViewModel.AllEvents.ErrorCode -> {
                        if (event.code == 1)
                            binding?.apply {
                                codeInput.error = "code should not be empty"
                            }
                        if (event.code == 2)
                            binding?.apply {
                                codeInput.error = "code length should be 8"
                            }

                    }

                    else ->{
                        Log.d(TAG, "listenToChannels: No event received so far")
                    }
                }

            }
        }
    }




}