package com.example.lodge

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class LodgeApp:Application()