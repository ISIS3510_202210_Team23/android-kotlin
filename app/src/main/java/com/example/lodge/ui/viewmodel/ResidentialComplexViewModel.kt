package com.example.lodge.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.AdminModel
import com.example.lodge.domain.complex.GetComplexAdministrators
import com.example.lodge.domain.complex.GetComplexBasicInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResidentialComplexViewModel @Inject constructor(
    private val getComplexBasicInfo: GetComplexBasicInfo,
    private val getComplexAdministrators: GetComplexAdministrators
): ViewModel() {
    private val TAG = "ResidentialComplexViewModel"
    val complexBasicInfo = MutableLiveData<Complex>()
    val complexAdmins = MutableLiveData<List<AdminModel>>()

    fun getComplexInfo(userId:String){
        viewModelScope.launch {
            val complexInfo: Complex = getComplexBasicInfo(userId = userId)
            complexBasicInfo.postValue(complexInfo)
        }
    }

    fun getComplexAdmins(userId:String){
        viewModelScope.launch {
            val admins: List<AdminModel> = getComplexAdministrators(userId = userId)
            complexAdmins.postValue(admins)
        }
    }
}