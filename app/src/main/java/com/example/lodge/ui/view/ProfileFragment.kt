package com.example.lodge.ui.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.lodge.R
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.databinding.FragmentUserProfileBinding
import com.example.lodge.ui.view.adapters.HommieItemAdapter
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.ui.viewmodel.ProfileViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream


@AndroidEntryPoint
class ProfileFragment: TrackedFragment() {

    override var trackingName: String = "ProfileFragment"

    private val USER_PREFERENCE_KEY = "USER"
    private val HOMMIES_PREFERENCE_KEY = "HOMMIES"
    private val registerViewModel : RegisterViewModel by activityViewModels()
    private val profileViewModel: ProfileViewModel by activityViewModels()
    private var _binding: FragmentUserProfileBinding? = null
    private val binding get() = _binding
    private lateinit var navController: NavController
    private var userId:String?=null
    private val PICK_IMAGE_REQUEST = 71


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        println("onCreateView")
        _binding = FragmentUserProfileBinding.inflate(inflater, container, false)




        var img: CircleImageView? = activity?.findViewById(R.id.userImage)
        if (img != null) {
            img.setImageResource(R.drawable.ic_baseline_account_circle_24)
        }
        navController = container?.let { Navigation.findNavController(it) }!!

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)

        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.VISIBLE
        }
        binding!!.noDataUser.visibility= View.GONE
        binding!!.noHommies.visibility=View.GONE



        listenToChannels()
        registerObservers()
        binding!!.userImage.setOnClickListener {
            activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
            }
            }

        }
        return binding?.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data == null || data.data == null) {
                return
            }
            userId?.let { profileViewModel.uploadUserPicture(it, data.data!!) }
        }
    }

    private fun registerObservers(){

        registerViewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                userId = user.uid
                val conn = context?.let { it1 -> NetworkHelper.checkForInternet(it1) }
                binding!!.profileLoading.visibility=View.VISIBLE
                if(conn != null){
                    if (conn){
                        println("bitmap")

                        profileViewModel.getUserInfo(userId = user.uid)

                    }
                    else{
                        Toast.makeText(requireContext(), "No connection, this info may not be up to date", Toast.LENGTH_LONG).show()

                        val sharedPref = activity?.getSharedPreferences(
                            USER_PREFERENCE_KEY, Context.MODE_PRIVATE)

                        val stringPref = sharedPref?.getString(USER_PREFERENCE_KEY,"")

                        val sharedPrefHommie = activity?.getSharedPreferences(
                            HOMMIES_PREFERENCE_KEY, Context.MODE_PRIVATE)

                        val stringPrefHommie = sharedPrefHommie?.getString(HOMMIES_PREFERENCE_KEY,"")
                        println("USERPREF: "+stringPref)
                        println("HOMMIEPREF: "+stringPrefHommie)

                        if(stringPref!=null && stringPref!="" && stringPrefHommie!=null && stringPrefHommie!=""){
                            val mapUser = stringUserToMap(stringPref)
                            val mapHommies = stringHommiesToMap(stringPrefHommie)

                            binding?.userUserName!!.text= mapUser["username"]
                            binding!!.userComplexName.text= mapUser["complex"]
                            binding!!.textUserName.text= mapUser["name"]
                            binding!!.textUserHome.text= mapUser["house"]
                            binding!!.textUserEmail.text= mapUser["email"]
                            binding!!.textUserGender.text= mapUser["gender"]
                            binding!!.textUserPhone.text= mapUser["phone"]
                            if(mapUser["userImage"]!= ""){
                                binding!!.userImage.setImageBitmap(mapUser["userImage"]?.let { it1 ->
                                    decodeStringImage(
                                        it1
                                    )
                                })
                            }
                            var hommies = mapHommies["hommies"] as ArrayList<UserModel>
                            if(hommies.size!=0){
                                val recycler = binding?.gridHommies
                                var listCache = mutableListOf<UserModel>()
                                for (item in hommies){
                                    listCache.add(UserModel(email = item.email, phone = item.phone, gender = item.gender, name = item.name, username = item.username, userImage = (if(item.userImage!= "")decodeStringImage(
                                        item.userImage as String
                                    ) else "")!!))
                                }
                                val adapter = context?.let { it1 -> HommieItemAdapter(listCache as ArrayList<UserModel>, it1) }
                                val layoutManager = GridLayoutManager(context,2)
                                if (recycler != null) {
                                    recycler.layoutManager= layoutManager
                                }
                                if (recycler != null) {
                                    recycler.adapter=adapter
                                }
                            }
                            else{
                                binding!!.noHommies.visibility=View.VISIBLE
                            }

                            binding!!.profileLoading.visibility=View.GONE

                        }
                        else{
                            binding!!.noDataUser.visibility = View.VISIBLE
                            binding?.userUserName!!.visibility= View.INVISIBLE
                            binding!!.userComplexName.visibility= View.INVISIBLE
                            binding!!.textUserName.visibility= View.INVISIBLE
                            binding!!.textUserHome.visibility= View.INVISIBLE
                            binding!!.textUserEmail.visibility= View.INVISIBLE
                            binding!!.textUserGender.visibility= View.INVISIBLE
                            binding!!.textUserPhone.visibility= View.INVISIBLE
                            binding!!.userImage.visibility = View.INVISIBLE

                            binding?.userBasicInfo!!.visibility= View.INVISIBLE
                            binding!!.gridHommies.visibility= View.INVISIBLE
                            binding!!.Hommietitle.visibility= View.INVISIBLE
                            binding!!.userEmail.visibility= View.INVISIBLE
                            binding!!.userGender.visibility= View.INVISIBLE
                            binding!!.userHouse.visibility= View.INVISIBLE
                            binding!!.userName.visibility= View.INVISIBLE
                            binding!!.userPhone.visibility= View.INVISIBLE
                            binding!!.profileLoading.visibility=View.GONE

                        }


                    }
                }

            }
        }
        profileViewModel.userInfoLive.observe(viewLifecycleOwner){

            val sharedPref = activity?.getSharedPreferences(
                USER_PREFERENCE_KEY, Context.MODE_PRIVATE)

            val image = if(it.userImage!= "")encodeImage(it.userImage as Bitmap) else ""

            val user = mapOf(
                "name" to it.name,
                "username" to it.username,
                "complex" to "",
                "house" to it.house,
                "email" to it.email,
                "gender" to it.gender,
                "phone" to it.phone,
                "userImage" to image
            )

            if(sharedPref != null){
                with(sharedPref.edit()){
                    putString(USER_PREFERENCE_KEY,mapUserToString(user as Map<String, String>))
                    apply()
                }
            }

            binding?.userUserName!!.text=it.username
            binding!!.userComplexName.text=it.complex
            binding!!.textUserName.text=it.name
            binding!!.textUserHome.text=it.house
            binding!!.textUserEmail.text=it.email
            binding!!.textUserGender.text=it.gender
            binding!!.textUserPhone.text=it.phone
            if(it.userImage!=""){
                binding!!.userImage.setImageBitmap(it.userImage as Bitmap?)
            }
            binding!!.profileLoading.visibility=View.GONE



        }
        profileViewModel.userHommies.observe(viewLifecycleOwner){

            val sharedPref = activity?.getSharedPreferences(
                HOMMIES_PREFERENCE_KEY, Context.MODE_PRIVATE)
           val cacheList = mutableListOf<UserModel>()
            for (item in it){
                cacheList.add(UserModel(email = item.email, phone = item.phone, gender = item.gender, name = item.name, username = item.username, userImage = (if(item.userImage!= "")encodeImage(item.userImage as Bitmap) else "")!!))
            }
            val list = mapOf(
                "hommies" to cacheList
            )

            if(sharedPref != null){
                with(sharedPref.edit()){
                    putString(HOMMIES_PREFERENCE_KEY,mapHommiesToString(list))
                    apply()
                }
            }

            if(it.isNotEmpty()){
                binding!!.noHommies.visibility=View.GONE
                val recycler = binding?.gridHommies
                val adapter = context?.let { it1 -> HommieItemAdapter(it as ArrayList<UserModel>, it1) }
                val layoutManager = GridLayoutManager(context,2)
                if (recycler != null) {
                    recycler.layoutManager= layoutManager
                }
                if (recycler != null) {
                    recycler.adapter=adapter
                }
            }
            else{
                binding!!.gridHommies.visibility= View.GONE
                binding!!.noHommies.visibility=View.VISIBLE
            }

        }
        profileViewModel.update.observe(viewLifecycleOwner){
            val sharedPref = activity?.getSharedPreferences(
                USER_PREFERENCE_KEY, Context.MODE_PRIVATE)

            val stringPref = sharedPref?.getString(USER_PREFERENCE_KEY,"")
            val bitmap = MediaStore.Images.Media.getBitmap(requireActivity().contentResolver, it)
            binding!!.userImage.setImageBitmap(bitmap)
            //binding!!.userImage.setImageResource(R.drawable.ic_baseline_account_circle_24)
            if(stringPref!=null && stringPref!=""){
                val mapUser = stringUserToMap(stringPref)
                val user = mapOf(
                    "name" to mapUser["name"],
                    "username" to mapUser["username"],
                    "complex" to mapUser["complex"],
                    "house" to mapUser["house"],
                    "email" to mapUser["email"],
                    "gender" to mapUser["gender"],
                    "phone" to mapUser["phone"],
                    "userImage" to encodeImage(bitmap)

                )
                with(sharedPref.edit()){
                    putString(USER_PREFERENCE_KEY,mapUserToString(user as Map<String, String>))
                    apply()
                }

            }

        }
        profileViewModel.complexInfo.observe(viewLifecycleOwner){
            val sharedPref = activity?.getSharedPreferences(
                USER_PREFERENCE_KEY, Context.MODE_PRIVATE)

            val stringPref = sharedPref?.getString(USER_PREFERENCE_KEY,"")
            binding!!.userComplexName.text=it.name
            if(stringPref!=null && stringPref!=""){
                val mapUser = stringUserToMap(stringPref)
                val user = mapOf(
                    "name" to mapUser["name"],
                    "username" to mapUser["username"],
                    "complex" to it.name,
                    "house" to mapUser["house"],
                    "email" to mapUser["email"],
                    "gender" to mapUser["gender"],
                    "phone" to mapUser["phone"],
                    "userImage" to mapUser["userImage"],

                    )
                with(sharedPref.edit()){
                    putString(USER_PREFERENCE_KEY,mapUserToString(user as Map<String, String>))
                    apply()
                }

            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.userImage.setImageResource(R.drawable.ic_baseline_account_circle_24)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding!!.gridHommies.visibility= View.GONE
    }



    private fun mapUserToString(map:Map<String, String>):String{
        val gson = Gson()
        return gson.toJson(map)
    }

    private fun stringUserToMap(str:String):Map<String, String>{
        val gson = Gson()
        return gson.fromJson(str, object : TypeToken<Map<String, String>>() {}.type)
    }

    private fun mapHommiesToString(map:Map<String, List<UserModel>>):String{
        val gson = Gson()
        return gson.toJson(map)
    }

    private fun stringHommiesToMap(str:String):Map<String, List<UserModel>>{
        val gson = Gson()
        return gson.fromJson(str, object : TypeToken<Map<String, List<UserModel>>>() {}.type)
    }
    private fun encodeImage(bm: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }
    private fun decodeStringImage(str: String):Bitmap {
        val decodedString: ByteArray = Base64.decode(str, Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        return decodedByte
    }
    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            profileViewModel.allEventsFlow.collect { event ->
                when (event) {
                    is ProfileViewModel.AllEvents.Error -> {
                        Toast.makeText(requireContext(), event.error, Toast.LENGTH_SHORT).show()
                    }
                    is ProfileViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                }
                }
            }
        }




}