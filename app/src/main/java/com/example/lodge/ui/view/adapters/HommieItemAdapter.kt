package com.example.lodge.ui.view.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.databinding.CardProfileHommiesBinding
import de.hdodenhof.circleimageview.CircleImageView

class HommieItemAdapter (
    private val values: ArrayList<UserModel>,
    private val context: Context
    ): RecyclerView.Adapter<HommieItemAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardProfileHommiesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            val item = values[position]
            holder.name.text=item.name
            if(item.userImage!=""){
                holder.img.setImageBitmap(item.userImage as Bitmap?)
            }
            holder.phone.setOnClickListener{
                val phoneInt = Intent(Intent.ACTION_DIAL)
                phoneInt.data=Uri.parse("tel:"+item.phone)
                context.startActivity(phoneInt)

            }



    }

    override fun getItemCount(): Int = values.size


    inner class ViewHolder(binding: CardProfileHommiesBinding):
        RecyclerView.ViewHolder(binding.root) {
        val name: TextView = binding.hommieName
        val phone: ImageButton = binding.callBtn
        val img: CircleImageView = binding.profileCardImg

        override fun toString(): String {
            return super.toString() + " '" + name.text + "'"
        }
    }
}