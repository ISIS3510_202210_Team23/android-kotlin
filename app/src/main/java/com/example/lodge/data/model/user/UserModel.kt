package com.example.lodge.data.model.user

import android.graphics.Bitmap

data class UserModel(
    val id:String="",
    val email:String="",
    val password:String="",
    val name:String="",
    val username:String="",
    val phone:String="",
    val gender:String="",
    val isAdmin:Boolean=false,
    val house:String="",
    val complex:String="",
    val userImage: Any
)
