package com.example.lodge.data.repository.complex

import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.AdminModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ComplexFirebase @Inject constructor(
    private val firestore: FirebaseFirestore
    ): BaseComplex {
    override suspend fun getComplexBasicInformation(complexId: String): Complex {
        return withContext(Dispatchers.IO){
            var complexDS: DocumentSnapshot = firestore.collection("ResidentialComplex").document(complexId).get().await()
            complexDS?.let {
                println()
                println("COMPLEX INFO $it")
                Complex(name = it.get("name") as String, city = it.get("city") as String, address = it.get("address") as String, emptyList<String>() )
            }
        }
    }

    override suspend fun getComplexAdministrators(complexId: String): List<AdminModel> {
        return withContext(Dispatchers.IO){
            var complexDS: DocumentSnapshot = firestore.collection("ResidentialComplex").document(complexId).get().await()
            complexDS?.let {
                var complexAdmin:HashMap<String, String> = it.get("admin") as HashMap<String, String>
                var adminId = if(complexAdmin["id"] != null) complexAdmin["id"] else ""
                var adminEmail = if(complexAdmin["email"] != null) complexAdmin["email"] else ""
                var adminPhone = if(complexAdmin["phone"] != null) complexAdmin["phone"] else ""
                var adminHouse = if(complexAdmin["house"] != null) complexAdmin["house"] else ""
                var adminName = if(complexAdmin["name"] != null) complexAdmin["name"] else ""

                listOf<AdminModel>(AdminModel(id = adminId!!, email = adminEmail!!, phone = adminPhone!!, house = adminHouse!!, name = adminName!!))
            }
        }
    }
    suspend fun getComplexByID(id:String):Complex{
        return withContext(Dispatchers.IO){
            var complex: DocumentSnapshot = firestore.collection("ResidentialComplex").document(id).get().await()
            var name:String = if(complex.get("name")!= null) complex.get("name") as String else ""
            var address:String = if(complex.get("address")!= null) complex.get("address") as String else ""
            var houses:List<String> = if(complex.get("houses")!=null) complex.get("houses") as List<String> else listOf()
            var city:String = if(complex.get("city")!= null) complex.get("city") as String else ""
            Complex(name = name, address = address, city = city, array = houses)


        }
    }
}