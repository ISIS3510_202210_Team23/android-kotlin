package com.example.lodge.ui.view.placeholder

import com.google.gson.annotations.SerializedName
import java.util.ArrayList
import java.util.HashMap

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object PlaceholderContent {

    /**
     * An array of sample (placeholder) items.
     */
    val ITEMS: MutableList<PlaceholderItem> = ArrayList()

    /**
     * A map of sample (placeholder) items, by ID.
     */
    val ITEM_MAP: MutableMap<String, PlaceholderItem> = HashMap()

    private val COUNT = 25

    init {
        // Add some sample items.
        for (i in 1..COUNT) {
            addItem(createPlaceholderItem(i))
        }
    }

    private fun addItem(item: PlaceholderItem) {
        ITEMS.add(item)
        ITEM_MAP.put(item.id, item)
    }

    private fun createPlaceholderItem(position: Int): PlaceholderItem {
        return PlaceholderItem(id=position.toString(), subject = "Administration fee", amount = 800000.0, paymentDateTime = "06:13", paymentStatus = "Pending")
    }

    /**
     * A placeholder item representing a piece of content.
     */
    data class PlaceholderItem(
        val id: String,
        val amount:Double,
        val paymentDateTime:String,
        val paymentStatus:String,
        val subject:String) {
    }
}