package com.example.lodge.data.repository.bot

import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BotFirebase  @Inject constructor(
    private val firestore: FirebaseFirestore
) {
    suspend fun createMessageBot(message: String){
        withContext(Dispatchers.IO){
            firestore.collection("Stats").document("statsFeature").update("messages",FieldValue.increment(1)).await()
        }
    }

}