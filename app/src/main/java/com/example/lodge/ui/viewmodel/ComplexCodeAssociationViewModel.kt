package com.example.lodge.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class ComplexCodeAssociationViewModel @Inject constructor(
    private val repoComplx: UserRepository
) : ViewModel(){

    private val TAG = "RegisterLoginViewModel"
    private val currentCode = MutableLiveData<String?>()
    val getCurrentCode get() = currentCode

    private val eventsChannel = Channel<AllEvents>()
    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()


    fun associateComplex(code: String, id:String) = viewModelScope.launch {

        when{
            code.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(1))
            }
            code.length!=8 -> {
                eventsChannel.send(AllEvents.ErrorCode(2))
            }

            else->{
                actualAssociateComplex(code,id)

            }
        }
    }

    private fun actualAssociateComplex(code:String, id:String) = viewModelScope.launch {

        try{

            val idComplex = repoComplx.associateComplex(code,id)
            if(idComplex!=null){
                val codeValue = code
                currentCode.postValue(codeValue)
                eventsChannel.send(AllEvents.Message("association to complex success"))
            }
            else{
                eventsChannel.send(AllEvents.Error("Invalid code. Code doesn't exist"))
            }

        }
        catch (e: Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "register complex: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }


    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }


}