package com.example.lodge.data.repository

import com.example.lodge.data.repository.bot.BotFirebase
import javax.inject.Inject

class BotRepository @Inject constructor(
    private val botFirebase: BotFirebase
) {

    suspend fun createMessageBot(message: String) = botFirebase.createMessageBot(message)


}