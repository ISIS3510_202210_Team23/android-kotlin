package com.example.lodge.data.cache

object UserMemCache {

    var name: String = ""
    var userName: String = ""
    var email: String = ""
    var password: String = ""
    var gender: String = ""
    var phone: String = ""
    var home: String = ""
    var complex: String = ""
    var isAdmin: Boolean = false

}