package com.example.lodge.domain.payment.model


import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.db.Bill
import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

data class BillView(
                    var id:String="",
                    val amount:Double=0.0,
                    val complex:String="",
                    val dueDate: Timestamp = Timestamp.now(),
                    val house: String="",
                    val name:String="",
                    val paid:Boolean=false,
                    val publishDate: Timestamp = Timestamp.now())

fun PaymentBillModel.toView() = BillView(id = id,amount=amount,complex=complex,dueDate=dueDate,name=name,publishDate=publishDate, house = house, paid = paid)
fun Bill.toView(): BillView {

    return BillView(id=id, amount=amount,complex=complex, dueDate = Timestamp(dueDate), name = name, publishDate = Timestamp(publishDate), house = house, paid = paid)
}
