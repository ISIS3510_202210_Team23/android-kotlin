package com.example.lodge.data.repository

import android.net.Uri
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.user.BaseUserClient
import com.example.lodge.data.repository.user.UserCreation
import com.example.lodge.data.repository.user.UserFirebaseClient
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class UserRepository @Inject constructor(
    private val repo : UserCreation,
    private val userClient: UserFirebaseClient
) {
    suspend fun createUser(id:String,email:String,name: String, username: String, phone: String, gender: String, isAdmin: Boolean){
        repo.createUser(id, email, name, username, phone, gender, isAdmin)
    }
    suspend fun createComplex(name: String, city: String, address: String, houses: List<String>,id:String){
        repo.createComplex(name, city, address, houses, id)
    }
    suspend fun associateComplex(code:String, id:String): String?{
        return repo.associateComplex(code,id)
    }

    suspend fun getUserById(userId:String): UserModel = userClient.getUserById(userId)
    suspend fun getComplexCoor(id: String): ArrayList<Double>? {
        return  repo.getComplexCoor(id)
    }
    suspend fun associateComplexVerification(id:String):Boolean?{
        return repo.associateComplexVerification(id)
    }
    suspend fun getHommies(userId:String,houseId:String):List<UserModel> = userClient.getHommies(userId,houseId)
    suspend fun uploadUserPicture(id:String, image:Uri):Uri=userClient.uploadUserPicture(id,image)







}