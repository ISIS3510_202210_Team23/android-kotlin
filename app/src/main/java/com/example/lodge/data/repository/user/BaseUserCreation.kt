package com.example.lodge.data.repository.user

import android.graphics.Bitmap
import android.net.Uri
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.UserModel

interface BaseUserCreation {

    suspend fun createUser(id:String,email:String,name: String, username: String, phone: String, gender: String, isAdmin: Boolean)

    suspend fun createComplex(name: String, city: String, address: String, houses: List<String>, id:String)

    suspend fun associateComplex(code: String, id:String):String?
    suspend fun getComplexCoor(id: String): ArrayList<Double>
    suspend fun associateComplexVerification(id:String): Boolean?


}