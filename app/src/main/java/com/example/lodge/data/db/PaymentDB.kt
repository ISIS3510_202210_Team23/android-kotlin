package com.example.lodge.data.db

import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.lodge.data.model.payment.db.Bill
import com.example.lodge.data.model.payment.db.Receipt

@Database(entities = [Bill::class,Receipt::class], version = 1)
@TypeConverters(Converters::class)
abstract class PaymentDB : RoomDatabase() {
    abstract fun paymentDao(): PaymentDao
    abstract fun billsDao(): BillsDao
}