package com.example.lodge.ui.view

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.lodge.R
import com.example.lodge.data.cache.BillsReceiptsMemCache
import com.example.lodge.data.model.geo.MyLocation
import com.example.lodge.data.repository.errors.GeofenceErrorMessages
import com.example.lodge.databinding.FragmentHomeBinding
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.ui.viewmodel.ComplexRegisterViewModel
import com.example.lodge.ui.viewmodel.HomeViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : TrackedFragment() {

    override var trackingName: String = "HomeFragment"

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding
    private val viewModel: RegisterViewModel by activityViewModels()
    private val viewModelHome : HomeViewModel by activityViewModels()
    private var currUser: String =""
    /*private var startTime: Long? = null
    private var endTime: Long? = null*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        println()
        //startTime=System.currentTimeMillis()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        getUser()
        activity?.let { binding?.let { it1 -> registerObserver(it, it1.root, inflater) } }
        listenToChannels()

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)

        if (viewTopBar != null) {
            viewTopBar.visibility = View.VISIBLE
        }
        if(viewBotBar != null) {
            viewBotBar.visibility=View.VISIBLE
        }



        return binding?.root
    }


    private fun getUser() {
        viewModel.getCurrentUser()
    }


    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.allEventsFlow.collect { event ->
                when (event) {
                    is RegisterViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    private fun registerObserver(context:Context, view: View, inflater:LayoutInflater) {
        viewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {

                binding?.apply {
                    welcomeTxt.text = "welcome ${it.email}"
                    signinButton.text = "sign out"
                    signinButton.setOnClickListener {
                        if(NetworkHelper.checkForInternet(context, view, inflater)) {
                            viewModel.signOut()
                        }
                    }
                }
            } ?: binding?.apply {
                welcomeTxt.text = "Welcome to Lodge"
                var viewBotBar: View?=activity?.findViewById(R.id.navigation)
                if (viewBotBar != null) {
                    viewBotBar.visibility=View.GONE
                }

                signinButton.text = "sign in"
                signinButton.setOnClickListener {
                    if(NetworkHelper.checkForInternet(context, view, inflater)){
                        findNavController().navigate(R.id.action_homeFragment_to_signInFragment)
                    }
                }
            }
        }

    }

}