package com.example.lodge.domain.profile

import android.net.Uri
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class uploadUserPictureUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    suspend operator fun invoke(id: String, image: Uri):Uri{
          return userRepository.uploadUserPicture(id,image)
    }

}