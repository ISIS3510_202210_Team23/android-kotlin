package com.example.lodge.data.repository.user

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.example.lodge.data.model.user.UserModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class UserFirebaseClient @Inject constructor(
    private val firestore: FirebaseFirestore,
    private val firebaseStorage: FirebaseStorage
): BaseUserClient {

    override suspend fun getUserById(userId: String): UserModel {
        return withContext(Dispatchers.IO) {
            var userDS:DocumentSnapshot = firestore.collection("Users").document(userId).get().await()
            println(userDS)

                var id:String = if(userDS.id != null) userDS.id else ""
                var email: String= if(userDS.get("email") != null) userDS.get("email") as String else ""
                var password: String= if(userDS.get("password") != null) userDS.get("password") as String else ""
                var name: String = if(userDS.get("name") != null) userDS.get("name") as String else ""
                var username: String= if(userDS.get("username") != null) userDS.get("username") as String else ""
                var phone: String= if(userDS.get("phone") != null) userDS.get("phone") as String else ""
                var gender: String= if(userDS.get("gender") != null) userDS.get("gender") as String else ""
                var isAdmin: Boolean= if(userDS.get("isAdmin") != null) userDS.get("isAdmin") as Boolean else false
                var house: String= if(userDS.get("house") != null) userDS.get("house") as String else ""
                var complex: String= if(userDS.get("complex") != null) userDS.get("complex") as String else ""
                var userImage = userDS.get("userImage")

                lateinit var bitmap:Any
                if(userImage!=null){
                    val ref = firebaseStorage.reference.child("UserProfile/"+userImage as String)
                    println("ref: "+ref)
                    val localFile = File.createTempFile(userImage as String,"")
                    println("file: "+localFile)
                    val res = ref.getFile(localFile).await()
                    println("RES: "+res)
                    bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                }else{
                    bitmap = ""
                }


                UserModel(
                    id =id,
                    email =email,
                    password =password,
                    name =name,
                    username =username,
                    phone =phone,
                    gender =gender,
                    isAdmin =isAdmin,
                    house = house,
                    complex = complex,
                    userImage = bitmap

                )


        }
    }
    override suspend fun getHommies(userId: String,houseId:String): List<UserModel> {
        return withContext(Dispatchers.IO){
            var list = mutableListOf<UserModel>()
            var house = firestore.collection("Houses").document(houseId).get().await()
            var houseMap = house.data
            println("MAPHOMMIE: "+houseMap)
            if(house.data?.get("users") !=null){
                var userIds:ArrayList<String> = houseMap?.get("users") as ArrayList<String>
                println("DATAHOMMIE: "+userIds)
                if(userIds.size!=0){
                    for (id in userIds){
                        if(id == userId){
                            continue
                        }
                        var user = firestore.collection("Users").document(id).get().await()
                        if(user.data?.get("userImage") !=null){
                            val ref = firebaseStorage.reference.child("UserProfile/"+user.data?.get("userImage"))
                            val localFile = File.createTempFile("userImage","jpg")
                            val res = ref.getFile(localFile).await()
                            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                            list.add(UserModel(email = user.data?.get("email") as String, phone = user.data?.get("phone") as String, gender = user.data?.get("gender") as String, name = user.data?.get("name") as String, username = user.data?.get("username") as String, userImage = bitmap))
                        }
                        else{
                            list.add(UserModel(email = user.data?.get("email") as String, phone = user.data?.get("phone") as String, gender = user.data?.get("gender") as String, name = user.data?.get("name") as String, username = user.data?.get("username") as String, userImage = ""))
                        }

                    }
                    println("LISTHOMMIES: "+list)
                    list
                }
                else{
                    list
                }
            }
            else{
                list
            }


        }
    }

    override suspend fun uploadUserPicture(id: String, image: Uri):Uri {
          return withContext(Dispatchers.IO){
            val refStorage = firebaseStorage.reference
            val idImg = UUID.randomUUID().toString()
            val ref = refStorage.child("UserProfile/"+ idImg)
            val uploadTask = ref.putFile(image).await()
            var user = firestore.collection("Users").document(id).update("userImage",idImg ).await()
            image

        }

    }

}