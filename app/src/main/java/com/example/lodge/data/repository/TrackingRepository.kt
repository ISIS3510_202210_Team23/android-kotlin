package com.example.lodge.data.repository

import com.example.lodge.data.repository.tracking.TrackingFirebase
import javax.inject.Inject

class TrackingRepository @Inject constructor(
    private val trackingClient: TrackingFirebase
) {

    suspend fun registerPeakHourTime(componentName:String, startTimeHour:String, duration:Long){
        trackingClient.registerPeakHourTrack(componentName, startTimeHour, duration)
    }
}