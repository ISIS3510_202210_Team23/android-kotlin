package com.example.lodge.data.model.payment.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.google.firebase.Timestamp
import com.google.gson.GsonBuilder
import com.google.gson.ToNumberPolicy
import java.util.*

@Entity(tableName = "receipts")
data class Receipt(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "dbId") val dbId: Int=0,
    @ColumnInfo(name = "idApi") var id: String="",
    @ColumnInfo(name = "bill") val bill: HashMap<String, Any> = HashMap<String, Any> (),
    @ColumnInfo(name = "date") val date: Date=Timestamp.now().toDate(),
    @ColumnInfo(name = "house") val house:String="",
    @ColumnInfo(name = "paymentOption") val paymentOption:HashMap<String, Any> = HashMap<String, Any> (),
    @ColumnInfo(name = "user") val user:String=""
)
fun PaymentReceiptModel.toDatabase(): Receipt {

    return Receipt(id=id, bill = bill, date = date.toDate(), house = house, paymentOption = paymentOption, user = user)
}
