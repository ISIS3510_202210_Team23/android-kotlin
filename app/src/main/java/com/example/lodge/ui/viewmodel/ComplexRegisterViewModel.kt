package com.example.lodge.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.repository.UserRepository
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class ComplexRegisterViewModel @Inject constructor(
    private val repoComplx: UserRepository
) : ViewModel() {

    private val TAG = "RegisterLoginViewModel"
    private val complex = MutableLiveData<Complex?>()
    val currentComplex get() = complex

    private val eventsChannel = Channel<AllEvents>()
    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()

    fun createComplex(name: String, city: String, address: String, id:String) = viewModelScope.launch {

        when{
            name.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(1))
            }
            name.length>30 -> {
                eventsChannel.send(AllEvents.ErrorCode(5))
            }
            name.length<6 -> {
                eventsChannel.send(AllEvents.ErrorCode(4))
            }
            city.isEmpty()-> {
                eventsChannel.send(AllEvents.ErrorCode(2))
            }
            city.length>30 -> {
                eventsChannel.send(AllEvents.ErrorCode(7))
            }
            city.length<3 -> {
                eventsChannel.send(AllEvents.ErrorCode(6))
            }
            address.isEmpty()->{
                eventsChannel.send(AllEvents.ErrorCode(3))
            }
            address.length>30 -> {
                eventsChannel.send(AllEvents.ErrorCode(9))
            }
            address.length<6 -> {
                eventsChannel.send(AllEvents.ErrorCode(8))
            }
            else->{
                actualCreateComplex(name,city,address,id)

            }
        }
    }


    private fun actualCreateComplex(name:String,city:String,address:String, id:String) = viewModelScope.launch {

        try{
            val houses = emptyList<String>()
            repoComplx.createComplex(name,city,address,houses,id)
            val resicomplex = Complex(name,city,address,houses)
            complex.postValue(resicomplex)
            eventsChannel.send(AllEvents.Message("create complex success"))
        }
        catch (e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "register complex: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }


    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }



}