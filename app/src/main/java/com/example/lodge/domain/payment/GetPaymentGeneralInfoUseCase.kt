package com.example.lodge.domain.payment

import com.example.lodge.data.repository.PaymentRepository
import com.example.lodge.data.model.payment.PaymentGeneralModel
import javax.inject.Inject

class GetPaymentGeneralInfoUseCase @Inject constructor(
    private val repository: PaymentRepository
) {
    suspend operator fun invoke(): PaymentGeneralModel = repository.getPaymentGeneralInfo()
}