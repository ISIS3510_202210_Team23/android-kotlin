package com.example.lodge.data.cache

import com.example.lodge.domain.payment.model.BillView
import com.example.lodge.domain.payment.model.ReceiptView

object BillsReceiptsMemCache {

    /**
     * List used for storing paged results of Bills
     */
    var completePagedBills: MutableList<BillView> = mutableListOf()

    /**
     * List used to store the first page of results in memory of Bills
     */
    var billsMemCache: MutableList<BillView> = mutableListOf()

    /**
     * List used for storing paged results of Receipts
     */
    var completePagedReceipts: MutableList<ReceiptView> = mutableListOf()

    /**
     * List used to store the first page of results in memory of Receipts
     */
    var receiptsMemCache: MutableList<ReceiptView> = mutableListOf()
}