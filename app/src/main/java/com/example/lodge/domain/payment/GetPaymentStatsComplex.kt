package com.example.lodge.domain.payment

import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.FinanceRepository
import com.example.lodge.data.repository.UserRepository
import com.example.lodge.domain.payment.model.ReceiptView
import javax.inject.Inject

class GetPaymentStatsComplex @Inject constructor(
    private val userRepository: UserRepository,
    private val financeRepository: FinanceRepository
) {


    suspend operator fun invoke(userId:String): Map<String,Int> {

        val user: UserModel =userRepository.getUserById(userId = userId)

        return financeRepository.getFinanceStats(user.complex)

    }
}