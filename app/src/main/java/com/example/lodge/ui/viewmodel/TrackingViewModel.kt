package com.example.lodge.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.domain.tracking.RegisterPeakHourTrack
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TrackingViewModel @Inject constructor(
    private val registerPeakHourTrack: RegisterPeakHourTrack
): ViewModel() {

    fun registerPickHours(componentName:String, startTimeHour:String, duration:Long){
        viewModelScope.launch {
            registerPeakHourTrack(componentName, startTimeHour, duration)
        }

    }
}