package com.example.lodge.data.repository.tracking

import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TrackingFirebase @Inject constructor(
    private val firestore: FirebaseFirestore
):BaseTracking {

    override suspend fun registerPeakHourTrack(componentName:String, startTimeHour:String, duration:Long) {
        println()
        println("TRACK $componentName - $startTimeHour - $duration")
        withContext(Dispatchers.IO){
            firestore.collection("Stats").document("statsPeakHours").update(
                mapOf(
                    "$componentName.$startTimeHour.count" to FieldValue.increment(1),
                    "$componentName.$startTimeHour.time" to FieldValue.increment(duration),
                )
            ).await()
            //"$componentName.$startTimeHour.count", FieldValue.increment(1)
        }
    }
}