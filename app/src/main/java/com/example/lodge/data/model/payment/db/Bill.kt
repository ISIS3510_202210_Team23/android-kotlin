package com.example.lodge.data.model.payment.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.lodge.data.model.payment.PaymentBillModel
import com.google.firebase.Timestamp
import java.util.*

@Entity(tableName = "bills")
data class Bill(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "dbId") val dbId: Int=0,
    @ColumnInfo(name = "idApi") var id:String="",
    @ColumnInfo(name = "amount") val amount:Double=0.0,
    @ColumnInfo(name = "complex") val complex:String="",
    @ColumnInfo(name = "dueDate") val dueDate: Date = Timestamp.now().toDate(),
    @ColumnInfo(name = "house") val house: String="",
    @ColumnInfo(name = "name") val name:String="",
    @ColumnInfo(name= "paid") val paid:Boolean=false,
    @ColumnInfo(name = "publishDate") val publishDate: Date = Timestamp.now().toDate()
)

fun PaymentBillModel.toDatabase() = Bill(id=id, amount = amount, complex = complex, dueDate = dueDate.toDate(), name = name, publishDate = publishDate.toDate(), house = house, paid = paid)
