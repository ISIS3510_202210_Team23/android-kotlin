package com.example.lodge.data.model.bot

data class Message(val message: String, val id: String, val time: String)
