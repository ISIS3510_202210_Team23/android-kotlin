package com.example.lodge.ui.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.repository.BaseAuthRepository
import com.example.lodge.data.repository.BotRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class BotViewModel @Inject constructor(
    private val botRepo: BotRepository
) : ViewModel(){

    private val TAG = "BotViewModel"
    private val eventsChannel = Channel<AllEvents>()
    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()


    fun createMessageBot(message:String) = viewModelScope.launch {

        try {
            botRepo.createMessageBot(message)
            eventsChannel.send(AllEvents.Message("message save succesfully"))
        }
        catch (e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "message save: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }


    }


    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }



}