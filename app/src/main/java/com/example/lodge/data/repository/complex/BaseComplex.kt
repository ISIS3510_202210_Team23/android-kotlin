package com.example.lodge.data.repository.complex

import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.AdminModel

interface BaseComplex {

    suspend fun getComplexBasicInformation(complexId:String): Complex

    suspend fun getComplexAdministrators(complexId: String): List<AdminModel>
}