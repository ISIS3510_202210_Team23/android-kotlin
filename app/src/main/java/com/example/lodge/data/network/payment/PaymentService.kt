package com.example.lodge.data.network.payment

import com.example.lodge.data.model.payment.PaymentGeneralModel
import com.example.lodge.data.model.payment.PaymentHistoryModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PaymentService @Inject constructor(
    private val api:PaymentApiClient
) {

    /**
     * Return the general information of the payment screens
     */
    suspend fun getPaymentGeneralInfo():PaymentGeneralModel?{
        return withContext(Dispatchers.IO){
            val response = api.getPaymentGeneralInfo()
            response.body()
        }
    }

    /**
     * Return the history of payments of the user
     */
    suspend fun getPaymentHistory():List<PaymentHistoryModel>?{
        return withContext(Dispatchers.IO){
            val response = api.getPaymentHistory()
            response.body()
        }
    }
}