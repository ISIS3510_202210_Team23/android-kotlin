package com.example.lodge.ui.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.lodge.R
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.databinding.FragmentPaymentHistoryListBinding
import com.example.lodge.databinding.FragmentResidentialComplexMainBinding
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.ui.viewmodel.ResidentialComplexViewModel
import com.example.lodge.utils.NetworkHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ResidentialComplexFragment :  TrackedFragment() {

    override var trackingName: String = "RegisterFragment"

    private val COMPLEX_PREFERENCE_KEY = "COMPLEX"

    private val complexViewModel: ResidentialComplexViewModel by activityViewModels()
    private val registerViewModel : RegisterViewModel by activityViewModels()
    private var _binding: FragmentResidentialComplexMainBinding? = null
    private val binding get() = _binding!!
    private var userId:String?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentResidentialComplexMainBinding.inflate(inflater, container, false)

        binding.adminCard.visibility=View.GONE
        binding.adminTitle.visibility=View.GONE

        binding.goToFinanceButton.setOnClickListener{
            //findNavController().navigate(R.id.action_residentialComplexFragment_to_financeActivity)
            activity?.let { it1 ->
                if (NetworkHelper.checkForInternet(it1, view, inflater)) {
                    findNavController().navigate(R.id.action_residentialComplexFragment_to_financeActivity)
                }
            }
        }

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)

        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.VISIBLE
        }

        binding.callButton.setOnClickListener {
            println("LLAMAR")
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerViewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                userId = user.uid
                val conn = context?.let { it1 -> NetworkHelper.checkForInternet(it1) }
                if (conn != null) {

                    binding.mapLogo.visibility = View.VISIBLE

                    if (conn) {
                        complexViewModel.getComplexInfo(user.uid)
                        complexViewModel.getComplexAdmins(user.uid)
                    } else {
                        Toast.makeText(requireContext(), "No connection, this info may not be up to date", Toast.LENGTH_LONG).show()
                        //Read from preferences
                        val sharedPref = activity?.getSharedPreferences(
                            COMPLEX_PREFERENCE_KEY, Context.MODE_PRIVATE)

                        val stringPref = sharedPref?.getString(COMPLEX_PREFERENCE_KEY,"")

                        if(stringPref!=null && stringPref!=""){
                            val complexPref: Complex = mapToComplex(stringToMap(stringPref) as Map<String, String>)
                            binding.apply {
                                residentialComplexName.text = complexPref.name
                                residentialComplexAddress.text = complexPref.address
                            }
                        }
                        else{
                            binding.apply {
                                residentialComplexName.text ="No disponible"
                                residentialComplexAddress.text = "No disponible"
                            }
                            binding.mapLogo.visibility = View.GONE

                        }
                    }
                }
            }
        }

        complexViewModel.complexAdmins.observe(viewLifecycleOwner) { admins ->
            println()
            println("ADMINS $admins")
            binding.apply {
                adminName.text = admins[0].name
            }
            binding.adminCard.visibility=View.VISIBLE
            binding.adminTitle.visibility=View.VISIBLE
        }

        complexViewModel.complexBasicInfo.observe(viewLifecycleOwner) { complex:Complex ->
            //Safe to preferences
            val sharedPref = activity?.getSharedPreferences(
                COMPLEX_PREFERENCE_KEY, Context.MODE_PRIVATE)

            if (sharedPref != null) {
                with (sharedPref.edit()) {
                    putString(COMPLEX_PREFERENCE_KEY, mapToString(complexToMap(complex)))
                    apply()
                }
            }

            binding.apply {
                residentialComplexName.text = complex.name
                residentialComplexAddress.text = complex.address
            }
        }
    }

    private fun complexToMap(complex:Complex): Map<String, String> {
        return mapOf("name" to complex.name, "address" to complex.address, "city" to complex.city)
    }

    private fun mapToComplex(map:Map<String, String>): Complex{
        return Complex(name = map["name"] as String, address = map["address"] as String, city = map["city"] as String, array = emptyList())
    }


    private fun mapToString(map:Map<String, Any>):String{
        val gson = Gson()
        return gson.toJson(map)
    }

    private fun stringToMap(str:String):Map<String, Any>{
        val gson = Gson()
        return gson.fromJson(str, object : TypeToken<Map<String, Any>>() {}.type)
    }

}