package com.example.lodge.ui.viewmodel

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.cache.UserMemCache
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.domain.profile.complexUseCase
import com.example.lodge.domain.profile.hommiesUseCase
import com.example.lodge.domain.profile.profileUseCase
import com.example.lodge.domain.profile.uploadUserPictureUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val profileUseCase: profileUseCase,
    private val hommiesUseCase: hommiesUseCase,
    private val uploadUserPictureUseCase: uploadUserPictureUseCase,
    private val complexUseCase: complexUseCase,
    private val userCache: UserMemCache
    ): ViewModel()  {

    private val eventsChannel = Channel<AllEvents>()
    val allEventsFlow = eventsChannel.receiveAsFlow()
    val userInfoLive = MutableLiveData<UserModel>()
    val userHommies = MutableLiveData<List<UserModel>>()
    val complexInfo = MutableLiveData<Complex>()
    val update = MutableLiveData<Uri>()

    fun getUserInfo(userId:String){
        viewModelScope.launch {
            try {
                val userInfo = profileUseCase(userId=userId)
                val complex = complexUseCase(userInfo.complex)
                val hommies = hommiesUseCase(userId=userId, houseId = userInfo.house)
                userInfoLive.postValue(userInfo)
                complexInfo.postValue(complex)
                userHommies.postValue(hommies)
            }
            catch (e:Exception){
                val error = e.toString().split(":").toTypedArray()
                Log.d("UserInfo", "UserInfo fail: ${error[1]}")
                eventsChannel.send(AllEvents.Error(error[1]))
            }

        }
    }
    fun uploadUserPicture(id:String, image:Uri){
        viewModelScope.launch {
            try {
                var uri= uploadUserPictureUseCase(id,image)
                var user = profileUseCase(userId = id)
                userInfoLive.postValue(user)
                //update.postValue(uri)
                eventsChannel.send(AllEvents.Message("profile photo uploaded"))
            }
            catch (e:Exception){
                val error = e.toString().split(":").toTypedArray()
                Log.d("Upload photo", "photo upload fail: ${error[1]}")
                eventsChannel.send(AllEvents.Error(error[1]))
            }

        }
    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }


}