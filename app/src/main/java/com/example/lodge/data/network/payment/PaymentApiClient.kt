package com.example.lodge.data.network.payment

import com.example.lodge.data.model.payment.PaymentGeneralModel
import com.example.lodge.data.model.payment.PaymentHistoryModel
import retrofit2.Response
import retrofit2.http.GET

interface PaymentApiClient {

    @GET("/paymentGeneral.json")
    suspend fun getPaymentGeneralInfo():Response<PaymentGeneralModel>

    @GET("/paymentHistory.json")
    suspend fun getPaymentHistory():Response<List<PaymentHistoryModel>>
}