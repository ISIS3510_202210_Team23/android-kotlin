package com.example.lodge.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.lodge.R
import com.example.lodge.databinding.FragmentPaymentMainBinding
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.AndroidEntryPoint
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@AndroidEntryPoint
class PaymentMainFragment: TrackedFragment() {

    override var trackingName: String = "PaymentMainFragment"

    private var _binding: FragmentPaymentMainBinding? = null
    private val binding get() = _binding!!
    private lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {





        _binding = FragmentPaymentMainBinding.inflate(inflater, container, false)


        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.VISIBLE
        }


        binding.mustPayButton.setOnClickListener{
            binding.paymentBillHistoryFragment.visibility=View.VISIBLE
            binding.paymentReceiptHistoryFragment.visibility=View.GONE
        }

        binding.paidButton.setOnClickListener{
            binding.paymentBillHistoryFragment.visibility=View.GONE
            binding.paymentReceiptHistoryFragment.visibility=View.VISIBLE
        }

        return binding.root

    }

    private fun formatDates(date: LocalDate?): String {
        if(date==null){
            return "Not available"
        }
        val formatter = DateTimeFormatter.ofPattern("dd MMM")
        return date.format(formatter)
    }

    override fun onDestroy() {

        super.onDestroy()
    }
}