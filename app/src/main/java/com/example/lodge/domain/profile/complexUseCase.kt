package com.example.lodge.domain.profile

import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.repository.ComplexRepository
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class complexUseCase @Inject constructor(private val complexRepository: ComplexRepository) {


    suspend operator fun invoke(complexId:String):Complex{
        return complexRepository.getComplexByID(complexId)
    }
}