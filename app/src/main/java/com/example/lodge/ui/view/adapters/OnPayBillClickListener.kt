package com.example.lodge.ui.view.adapters

interface OnPayBillClickListener {

    fun onPayBillClickListener(amount:String)
}