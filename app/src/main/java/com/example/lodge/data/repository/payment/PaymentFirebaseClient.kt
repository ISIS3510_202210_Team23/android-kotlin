package com.example.lodge.data.repository.payment

import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.data.model.payment.db.Bill
import com.example.lodge.data.model.payment.db.Receipt
import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import org.w3c.dom.Document
import javax.inject.Inject

class PaymentFirebaseClient @Inject constructor(
    private val firestore: FirebaseFirestore
): BasePaymentClient {

    override suspend fun getBillsByHouse(houseId: String, idOffset: String?, limit: Int):List<PaymentBillModel>{
        return withContext(Dispatchers.IO) {
            if (idOffset==null){
                firestore.collection("Bills")
                    .whereEqualTo("house", houseId)
                    .whereEqualTo("paid", false)
                    .orderBy("publishDate", Query.Direction.DESCENDING)
                    .limit(limit.toLong())
                    .get().await().map {
                    var obj = it.toObject(PaymentBillModel::class.java)
                    obj.id = it.id
                    obj
                }
            }
            else{
                var doc:DocumentSnapshot=firestore.collection("Bills").document(idOffset).get().await()
                firestore.collection("Bills")
                    .whereEqualTo("house", houseId)
                    .whereEqualTo("paid", false)
                    .orderBy("publishDate", Query.Direction.DESCENDING)
                    .startAfter(doc)
                    .limit(limit.toLong())
                    .get().await().map {
                    var obj = it.toObject(PaymentBillModel::class.java)
                    obj.id = it.id
                    obj
                }
            }
        }
    }

    override suspend fun getBillsByResidentialComplex(complexId: String): List<PaymentBillModel> {
        return withContext(Dispatchers.IO) {
            firestore.collection("Bills")
                .whereEqualTo("complex", complexId)
                .get()
                .await()
                .map {
                var obj = it.toObject(PaymentBillModel::class.java)
                obj.id = it.id
                obj
            }
        }
    }

    override suspend fun getReceiptsPaidByUser(userId:String, houseId:String, idOffset: String?, limit: Int): List<PaymentReceiptModel>{
        return withContext(Dispatchers.IO) {
            if (idOffset==null){
                firestore.collection("Receipts")
                    .whereEqualTo("house", houseId)
                    .whereEqualTo("user", userId)
                    .orderBy("date", Query.Direction.DESCENDING)
                    .limit(limit.toLong())
                    .get()
                    .await()
                    .map{
                        var obj = it.toObject(PaymentReceiptModel::class.java)
                        obj.id = it.id
                        obj
                    }

            }
            else{
                var doc:DocumentSnapshot=firestore.collection("Receipts").document(idOffset).get().await()
                firestore.collection("Receipts")
                    .whereEqualTo("house", houseId)
                    .whereEqualTo("user", userId)
                    .orderBy("date", Query.Direction.DESCENDING)
                    .startAfter(doc)
                    .limit(limit.toLong())
                    .get()
                    .await()
                    .map{
                        var obj = it.toObject(PaymentReceiptModel::class.java)
                        obj.id = it.id
                        obj
                    }
            }


        }
    }

    override suspend fun createReceiptPaidBill(amount:String, userId: String, houseId:String, bill:PaymentBillModel ): PaymentReceiptModel{
        return withContext(Dispatchers.IO) {

            val billMap: HashMap<String, Any> = HashMap<String, Any>()
            billMap.put("id", bill.id)
            billMap.put("amount", bill.amount)
            billMap.put("complex", bill.complex)
            billMap.put("dueDate", bill.dueDate)
            billMap.put("name", bill.name)
            billMap.put("publishDate", bill.publishDate)
            billMap.put("paid", true)

            val paymentOption: HashMap<String, Any> = hashMapOf(
                "description" to "Pay with google pay with your credit or debit card",
                "name" to "GooglePay"
            )
            val data= hashMapOf(
                "bill" to billMap,
                "user" to userId,
                "date" to Timestamp.now(),
                "house" to houseId,
                "paymentOption" to paymentOption,

            )

            firestore.collection("Receipts").add(data).await()
            firestore.collection("Bills").document(bill.id).update("paid", true).await()
            firestore.collection("Stats").document("statsFeature").update("countPayments",FieldValue.increment(1)).await()
            PaymentReceiptModel(date = Timestamp.now(), bill = billMap, paymentOption = paymentOption, house = houseId, user = userId)
        }
    }

    override suspend fun getBillById(billId: String): PaymentBillModel {
        return withContext(Dispatchers.IO) {
            firestore.collection("Bills").document(billId).get().await().toObject(PaymentBillModel::class.java)!!
        }
    }
}