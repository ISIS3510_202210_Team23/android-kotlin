package com.example.lodge.ui.view

 import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
 import android.widget.Toast
 import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Pie
import com.example.lodge.R
import com.example.lodge.databinding.FinanceActivityBinding
 import com.example.lodge.ui.view.trackers.TrackedFragment
 import com.example.lodge.ui.viewmodel.FinanceViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FinanceFragment: TrackedFragment()  {

    override var trackingName: String = "FinanceFragment"

    private val STATS_PREFERENCE_KEY = "STATS"

    private val viewModel: FinanceViewModel by activityViewModels()
    private val registerViewModel : RegisterViewModel by activityViewModels()
    private var _binding: FinanceActivityBinding? = null
    private val binding get() = _binding
    private var chart: AnyChartView? = null
    private val pays = arrayListOf("Paid","Not Paid")
    private var vals = arrayListOf(0.0,0.0)
    private var userId:String?=null


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        println()
        println("FINANCE CREATE VIEW")
        _binding = FinanceActivityBinding.inflate(inflater,container,false)

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)

        if (viewTopBar != null) {
            viewTopBar.visibility = View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility = View.GONE
        }

        binding?.charProgressBar?.visibility=View.GONE
        binding?.noDataMessage?.visibility=View.GONE


        chart = binding?.pieChart
        return binding?.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerViewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                userId = user.uid
                val conn = context?.let { it1 -> NetworkHelper.checkForInternet(it1) }
                if (conn != null) {

                    if (conn){
                        binding?.noDataMessage?.visibility = View.INVISIBLE
                        binding?.pieChart?.visibility = View.INVISIBLE
                        binding?.charProgressBar?.visibility = View.VISIBLE
                        viewModel.financeEvent(user.uid)
                    }
                    else{
                        Toast.makeText(requireContext(), "No connection, this info may not be up to date", Toast.LENGTH_LONG).show()
                        //Read from preferences
                        val sharedPref = activity?.getSharedPreferences(
                            STATS_PREFERENCE_KEY, Context.MODE_PRIVATE)

                        val stringPref = sharedPref?.getString(STATS_PREFERENCE_KEY,"")

                        if(stringPref!=null && stringPref!=""){
                            val mapPref = stringToMap(stringPref)
                            setUpChartFromMap(mapPref)
                        }
                        else{
                            //Use default
                            setUpChartFromMap(null)
                        }
                    }
                }
            }
        }

        viewModel.complexStats.observe(viewLifecycleOwner){ stats ->
            //Safe to preferences
            val sharedPref = activity?.getSharedPreferences(
                STATS_PREFERENCE_KEY, Context.MODE_PRIVATE)

            if (sharedPref != null) {
                with (sharedPref.edit()) {
                    putString(STATS_PREFERENCE_KEY, mapToString(stats))
                    apply()
                }
            }

            setUpChartFromMap(stats)

            binding?.noDataMessage?.visibility = View.INVISIBLE
            binding?.pieChart?.visibility = View.VISIBLE
            binding?.charProgressBar?.visibility = View.INVISIBLE
        }
    }


    private fun mapToString(map:Map<String, Int>):String{
        val gson = Gson()
        return gson.toJson(map)
    }

    private fun stringToMap(str:String):Map<String, Int>{
        val gson = Gson()
        return gson.fromJson(str, object : TypeToken<Map<String, Int>>() {}.type)
    }

    private fun setUpChartFromMap(map: Map<String, Int>?){
        if (map!=null){
            var paidQuantity = map["Paid"]?.toDouble()
            var notPaidQuantity = map["NotPaid"]?.toDouble()
            var total = paidQuantity?.plus(notPaidQuantity!!)
            var valPaid = (paidQuantity?.div(total!!))?.times(100)
            var valNotPaid = (notPaidQuantity!! / total!!)*100

            if (vals[0]!=valPaid || vals[1]!=valNotPaid)
            {
                vals[0] = (paidQuantity!! / total!!)*100
                vals[1] = (notPaidQuantity!! / total!!)*100

                val pie:Pie = AnyChart.pie()
                var list = ArrayList<DataEntry>()
                for ((index,item) in pays.withIndex()){
                    list.add(ValueDataEntry(item, vals[index]))
                }
                pie.data(list)
                val percent:TextView = binding?.textPercent ?: TextView(context)
                var porcentaje = if(vals[0]!=0.0 && vals[1]!=0.0) (vals[0]/(vals[0]+vals[1]))*100 else 0
                percent.text="${porcentaje}%"
                chart?.setChart(pie)
            }
        }
        else{
            //No data message
            binding?.noDataMessage?.visibility = View.VISIBLE
            binding?.charProgressBar?.visibility = View.INVISIBLE
            binding?.pieChart?.visibility = View.INVISIBLE
            val percent:TextView = binding?.textPercent ?: TextView(context)
            percent.text="-"
        }
    }
}