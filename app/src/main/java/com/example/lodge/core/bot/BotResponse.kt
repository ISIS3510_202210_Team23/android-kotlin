package com.example.lodge.core.bot

import com.example.lodge.data.model.bot.Constants.OPEN_GOOGLE
import com.example.lodge.data.model.bot.Constants.OPEN_SEARCH
import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

object BotResponse {

    fun basicResponses(_message: String): String {

        val random = (0..2).random()
        val message = _message.lowercase(Locale.getDefault())

        return when {

            //Flips a coin
            message.contains("flip") && message.contains("coin") -> {
                val r = (0..1).random()
                val result = if (r == 0) "heads" else "tails"

                "I flipped a coin and it landed on $result"
            }

            //Math calculations
            message.contains("solve") -> {
                val equation: String? = message.substringAfterLast("solve")
                return try {
                    val answer = SolveMath.solveMath(equation ?: "0")
                    "$answer"

                } catch (e: Exception) {
                    "Sorry, I can't solve that."
                }
            }

            //Hello
            message.contains("hello") -> {
                when (random) {
                    0 -> "Hello there!"
                    1 -> "Sup"
                    2 -> "Buongiorno!"
                    else -> "error" }
            }

            //How are you?
            message.contains("how are you") -> {
                when (random) {
                    0 -> "I'm doing fine, thanks!"
                    1 -> "I'm hungry..."
                    2 -> "Pretty good! How about you?"
                    else -> "error"
                }
            }

            //What time is it?
            message.contains("time") && message.contains("?")-> {
                val timeStamp = Timestamp(System.currentTimeMillis())
                val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm")
                val date = sdf.format(Date(timeStamp.time))

                date.toString()
            }

            //Open Google

            message.contains("1") -> {
                "Monday,Tusday and Friday from 13:00 to 18:00"
            }
            message.contains("2") -> {
                "Monday,Wednesday and Friday"
            }
            message.contains("3") -> {
                "July 12 of 2022 at 9 a.m"
            }
            message.contains("4") -> {
                "May 2 of 2022 at 16:00 p.m"
            }

            //When the programme doesn't understand...
            else -> {
                "Hi I'm Lodge Bot I can help you with the next questions (Type the number question): \n1. ¿Which are the administrator attention hours? \n2. ¿Which days does the garbage collector comes? \n3. ¿When is the next complex board meeting? \n 4. ¿When is the next complex event?"

            }
        }
    }



}