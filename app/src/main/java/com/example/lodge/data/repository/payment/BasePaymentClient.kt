package com.example.lodge.data.repository.payment

import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.data.model.payment.db.Bill
import com.example.lodge.data.model.payment.db.Receipt
import com.google.firebase.firestore.DocumentSnapshot

interface BasePaymentClient {

    suspend fun getBillsByHouse(houseId: String, idOffset: String?, limit: Int):List<PaymentBillModel>

    suspend fun getBillById(billId:String):PaymentBillModel

    suspend fun getBillsByResidentialComplex(complexId:String): List<PaymentBillModel>

    suspend fun getReceiptsPaidByUser(userId:String, houseId:String, idOffset: String?, limit: Int): List<PaymentReceiptModel>

    suspend fun createReceiptPaidBill(amount:String, userId: String, houseId:String, bill:PaymentBillModel ): PaymentReceiptModel

}