package com.example.lodge.domain.tracking

import android.net.Uri
import com.example.lodge.data.repository.TrackingRepository
import javax.inject.Inject

class RegisterPeakHourTrack @Inject constructor(
    private val trackRepository: TrackingRepository
){

    suspend operator fun invoke(componentName:String, startTimeHour:String, duration:Long) {
        trackRepository.registerPeakHourTime(componentName, startTimeHour, duration)
    }
}