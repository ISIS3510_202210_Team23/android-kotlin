package com.example.lodge.ui.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.lodge.R
import com.example.lodge.databinding.RegisterFragmentBinding
import com.example.lodge.ui.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.navigation.fragment.findNavController
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.utils.NetworkHelper
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class RegisterFragment : TrackedFragment() {

    override var trackingName: String = "RegisterFragment"


    private val viewModel : RegisterViewModel by activityViewModels()
    private var _binding : RegisterFragmentBinding? = null
    private val binding get()  = _binding
    private val TAG = "SignUpFragment"
    private var admin = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RegisterFragmentBinding.inflate(inflater , container , false)

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.GONE
        }

        registerObservers()
        listenToChannels()
        binding?.apply {
            buttonCreate.setOnClickListener {

                activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                    val email = emailInput.text.toString()
                    val password = passInput.text.toString()
                    val confirmPass = confPassInput.text.toString()
                    val name = nameInput.text.toString()
                    val username = usernameInput.text.toString()
                    val phone = phoneInput.text.toString()
                    val gender = toggleButtonGender.checkedButtonId
                    var strGender=""
                    if(gender!=-1){
                        genderLabel.error=null
                        if(gender==2131362086){
                            strGender="Male"
                        }
                        else{
                            strGender="Female"
                        }
                    }
                    else{
                        strGender="-1"
                        genderLabel.error="Choose a gender"
                    }

                    val isAdmin = switchAdmin.isChecked
                    admin = isAdmin

                    println(admin)
                    viewModel.signUpUser(email , password , confirmPass, name, username, phone,strGender,isAdmin)

                }
                }



            }

            SingIn.setOnClickListener {
                findNavController().navigate(R.id.action_signUpFragment_to_signInFragment)
            }

        }

        return binding?.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }


    private fun registerObservers() {
        viewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                findNavController().navigate(R.id.action_complexCodeFragment_to_homeFragment)

            }
        }
    }

    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.allEventsFlow.collect { event ->
                when(event){
                    is RegisterViewModel.AllEvents.Error -> {
                        binding?.apply {
                            errText.text = event.error
                        }
                    }
                    is RegisterViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                    is RegisterViewModel.AllEvents.ErrorCode -> {
                        if (event.code == 1)
                            binding?.apply {
                                emailInput.error = "email should not be empty"
                            }
                        if(event.code == 2)
                            binding?.apply {
                                passInput.error = "password should not be empty"
                            }

                        if(event.code == 3)
                            binding?.apply {
                                confPassInput.error = "passwords do not match"
                            }
                        if(event.code ==4)
                            binding?.apply {
                                emailInput.error = "email should have less than 30 characters"
                            }
                        if(event.code ==5)
                            binding?.apply {
                                emailInput.error = "email should have at least 7 characters"
                            }
                        if(event.code ==6)
                            binding?.apply {
                                passInput.error = "password should have less than 30 characters"
                            }
                        if(event.code ==17)
                            binding?.apply {
                                passInput.error = "password should have at least 6 characters"
                            }
                        if(event.code ==7)
                            binding?.apply {
                                emailInput.error = "email doesnt have the correct structure"
                            }
                        if(event.code ==8)
                            binding?.apply {
                                nameInput.error = "name should have less than 30 characters"
                            }
                        if(event.code ==9)
                            binding?.apply {
                                nameInput.error = "name should have at least 6 characters"
                            }
                        if(event.code ==10)
                            binding?.apply {
                                usernameInput.error = "username should have less than 30 characters"
                            }
                        if(event.code ==11)
                            binding?.apply {
                                usernameInput.error = "username should have at least 6 characters"
                            }
                        if(event.code ==12)
                            binding?.apply {
                                phoneInput.error = "phone doesnt matches the structure (###-###-####)"
                            }
                        if(event.code ==13)
                            binding?.apply {
                                nameInput.error = "user cant be empty"
                            }
                        if(event.code ==14)
                            binding?.apply {
                                usernameInput.error = "username cant be empty"
                            }
                        if(event.code ==15)
                            binding?.apply {
                                phoneInput.error = "phone cant be empty"
                            }
                        if(event.code ==16){

                        }

                        if(event.code ==18)
                            binding?.apply {
                                genderLabel.error = null
                            }
                        if(event.code==100){

                            if(admin){
                                findNavController().navigate(R.id.action_signUpFragment_to_createComplexFragment)
                            }
                            else{
                                findNavController().navigate(R.id.action_signUpFragment_to_complexCodeFragment)
                            }
                        }

                    }

                    else ->{
                        Log.d(TAG, "listenToChannels: No event received so far")
                    }
                }

            }
        }
    }



}