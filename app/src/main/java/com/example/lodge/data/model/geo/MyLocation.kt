package com.example.lodge.data.model.geo

import java.util.*

data class MyLocation(val key: String, val latitude: Double, val longitude: Double)
