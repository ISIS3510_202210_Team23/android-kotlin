package com.example.lodge.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.repository.FinanceRepository
import com.example.lodge.domain.payment.GetPaymentStatsComplex
import com.example.lodge.domain.payment.model.BillView
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FinanceViewModel @Inject constructor(
    private val financeRepo: FinanceRepository,
    private val getPaymentStatsComplex: GetPaymentStatsComplex
): ViewModel() {
    private val TAG = "BotViewModel"
    private val eventsChannel = Channel<AllEvents>()
    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()

    val complexStats = MutableLiveData<Map<String,Int>>()

    fun financeEvent(userId:String) = viewModelScope.launch {
        val mapa=getPaymentStatsComplex(userId = userId)
        complexStats.postValue(mapa)

        try {
            financeRepo.financeEvent()
            eventsChannel.send(AllEvents.Message("finance event save succesfully"))
        }
        catch (e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "finance event save: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }
    }

    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }
}