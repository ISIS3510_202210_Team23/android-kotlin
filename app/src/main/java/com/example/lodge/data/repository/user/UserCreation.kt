package com.example.lodge.data.repository.user

import com.example.lodge.data.cache.UserMemCache
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.UserModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.SetOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.random.Random

class UserCreation @Inject constructor(
    private val firestore: FirebaseFirestore
) : BaseUserCreation {
    @Inject
    lateinit var userInfo:UserMemCache
    override suspend fun createUser(
        id: String,
        email: String,
        name: String,
        username: String,
        phone: String,
        gender: String,
        isAdmin: Boolean
    ) {
        withContext(IO){
            val user = hashMapOf(
                "name" to name,
                "username" to username,
                "email" to email,
                "phone" to phone,
                "gender" to gender,
                "isAdmin" to isAdmin
            )
            firestore.collection("Users").document(id).set(user, SetOptions.merge()).await()
        }

    }


    override suspend fun createComplex(
        name: String,
        city: String,
        address: String,
        houses: List<String>,
        id: String
    ) {
        withContext(IO){
            val complex = hashMapOf(
                "name" to name,
                "city" to city,
                "address" to address,
                "houses" to houses

            )
            val user = hashMapOf(
                "name" to userInfo.name,
                "username" to userInfo.userName,
                "email" to userInfo.email,
                "phone" to userInfo.phone,
                "gender" to userInfo.gender,
                "isAdmin" to userInfo.isAdmin,
            )
            val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
            var idComplex = (1..20).map { allowedChars.random() }.joinToString("")
            user.put("complex",idComplex)
            firestore.collection("ResidentialComplex").document(idComplex).set(complex, SetOptions.merge()).await()
            firestore.collection("Users").document(id).set(user, SetOptions.merge()).await()
        }

    }

    override suspend fun associateComplex(code:String, id:String): String? {
        return withContext(IO){
            val user = hashMapOf(
                "name" to userInfo.name,
                "username" to userInfo.userName,
                "email" to userInfo.email,
                "phone" to userInfo.phone,
                "gender" to userInfo.gender,
                "isAdmin" to userInfo.isAdmin,
            )
            var house = firestore.collection("Houses").document(code).get().await()
            if (house.exists()){
                //firestore.collection("Users").document(id).update("house",code).await()
                var houseMap = house.data
                if (houseMap != null) {
                    var idComplex = houseMap["complex"].toString()
                    user.put("house",code)
                    user.put("complex",idComplex)
                    firestore.collection("Users").document(id).set(user, SetOptions.merge()).await()
                    idComplex
                }
                else{
                    null
                }
            }
            else
                null
        }


    }
    override suspend fun associateComplexVerification(code:String): Boolean? {
        return withContext(IO){

            var house = firestore.collection("Houses").document(code).get().await()
            house.exists()
        }


    }

    override suspend fun getComplexCoor(id: String): ArrayList<Double> {
        return withContext(IO){
            var user = firestore.collection("Users").document(id).get().await()
            var userData = user.data
            var complexId = userData?.get("complex").toString()
            var complex= firestore.collection("ResidentialComplex").document(complexId).get().await()
            var complexData = complex.data
            var coord:GeoPoint = complexData?.get("location") as GeoPoint
            var lat = coord.latitude
            var lon = coord.longitude
            var arrCoord = ArrayList<Double>()
            arrCoord.add(lat)
            arrCoord.add(lon)
            arrCoord
        }

    }








}