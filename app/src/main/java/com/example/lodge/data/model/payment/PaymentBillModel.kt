package com.example.lodge.data.model.payment

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.lodge.data.model.payment.db.Bill
import com.google.firebase.Timestamp


data class PaymentBillModel(
    var id:String="",
    val amount:Double=0.0,
    val complex:String="",
    val dueDate: Timestamp=Timestamp.now(),
    val house: String="",
    val name:String="",
    val paid:Boolean=false,
    val publishDate: Timestamp=Timestamp.now()
)

