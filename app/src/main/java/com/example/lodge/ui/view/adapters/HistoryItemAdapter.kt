package com.example.lodge.ui.view.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.example.lodge.data.model.payment.PaymentHistoryModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.databinding.FragmentPaymentHistoryItemBinding
import com.example.lodge.domain.payment.model.ReceiptView

import com.example.lodge.ui.view.placeholder.PlaceholderContent.PlaceholderItem
import com.google.firebase.Timestamp
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class HistoryItemAdapter(
    private val values: List<ReceiptView>
) : RecyclerView.Adapter<HistoryItemAdapter.ViewHolder>() {

    /**
     * Create new views (invoked by the layout manager)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //println("adapter")
        return ViewHolder(
            FragmentPaymentHistoryItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        if(item.bill.get("name")!=null){
            holder.subjectView.text = item.bill.get("name") as String
        }
        holder.amountView.text = item.house
        holder.historyTimeView.text = item.date.toDate().toString()
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     */
    override fun getItemCount(): Int = values.size

    /**
     * Provide a reference to the views for each data item
        Complex data items may need more than one view per item, and
        you provide access to all the views for a data item in a view holder.
        Each data item is just an Affirmation object.
      */
    inner class ViewHolder(binding: FragmentPaymentHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val subjectView: TextView = binding.historyItemSubject
        val amountView: TextView = binding.historyItemAmount
        val historyTimeView: TextView = binding.historyItemTime

        override fun toString(): String {
            return super.toString() + " '" + subjectView.text + "'"
        }
    }

}