package com.example.lodge.ui.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.lodge.R
import com.example.lodge.databinding.LoginFragmentBinding
import com.example.lodge.ui.viewmodel.RegisterViewModel
import dagger.hilt.android.AndroidEntryPoint
import androidx.navigation.fragment.findNavController
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.utils.NetworkHelper
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class LoginFragment: TrackedFragment() {

    override var trackingName: String = "LoginFragment"

    private val viewModel : RegisterViewModel by activityViewModels()
    private var _binding : LoginFragmentBinding? = null
    private val binding get() = _binding
    private val TAG = "SignInFragment"
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = LoginFragmentBinding.inflate(inflater , container , false)
        listenToChannels()
        registerObservers()

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.GONE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.GONE
        }

        binding?.apply {
            LoginButton.setOnClickListener {
                activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                    val email = UsernameField.text.toString()
                    val password = PasswordField.text.toString()
                    viewModel.signInUser(email, password)
                }
                }

            }

            signIn.setOnClickListener {

                findNavController().navigate(R.id.action_signInFragment_to_signUpFragment)
            }


        }
        return binding?.root
    }

    private fun registerObservers() {
        viewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                findNavController().navigate(R.id.action_signInFragment_to_homeFragment)
            }
        }
    }

    private fun listenToChannels() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.allEventsFlow.collect { event ->
                when(event){
                    is RegisterViewModel.AllEvents.Error -> {
                        binding?.apply {
                            errorText.text =  event.error
                        }
                    }
                    is RegisterViewModel.AllEvents.Message -> {
                        Toast.makeText(requireContext(), event.message, Toast.LENGTH_SHORT).show()
                    }
                    is RegisterViewModel.AllEvents.ErrorCode -> {
                        if (event.code == 1)
                            binding?.apply {
                                UsernameField.error = "email should not be empty"
                            }
                        if(event.code == 2)
                            binding?.apply {
                                PasswordField.error = "password should not be empty"
                            }
                        if(event.code == 3)
                            binding?.apply {
                                UsernameField.error = "email should have less than 30 characters"
                            }
                        if(event.code == 4)
                            binding?.apply {
                                UsernameField.error = "email should have at least 7 characters"
                            }
                        if(event.code == 5)
                            binding?.apply {
                                PasswordField.error = "password should have at least 6 characters"
                            }
                        if(event.code == 6)
                            binding?.apply {
                                PasswordField.error = "password should have less than 30 characters"
                            }
                        if(event.code == 7)
                            binding?.apply {
                                UsernameField.error = "Email format invalid"
                            }

                    }

                    else ->{
                        Log.d(TAG, "listenToChannels: No event received so far")
                    }
                }

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}