package com.example.lodge.data.repository

import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.model.user.AdminModel
import com.example.lodge.data.repository.complex.ComplexFirebase
import javax.inject.Inject

class ComplexRepository @Inject constructor(
    private val complexService: ComplexFirebase
) {

    suspend fun getComplexBasicInformation(complexId:String): Complex {
        return complexService.getComplexBasicInformation(complexId)
    }

    suspend fun getComplexAdministrators(complexId: String): List<AdminModel> {
        return complexService.getComplexAdministrators(complexId)
    }

    suspend fun getComplexByID(id:String): Complex =complexService.getComplexByID(id)
}