package com.example.lodge.data.model.payment

import javax.inject.Inject
import javax.inject.Singleton

/**
 * This class works a cache container for the information retrieved initially from the internet
 */
@Singleton
class PaymentProvider @Inject constructor() {
        var paymentGeneralInfo = PaymentGeneralModel(
            balance=1250800.90,
            closingDate = "2022-03-26",
            dueDate = "2022-03-26"
        )

        var paymentHistory:List<PaymentHistoryModel> = emptyList()

        var paymentReceiptHistory:List<PaymentReceiptModel> = emptyList()

        var paymentBillHistory:List<PaymentBillModel> = emptyList()
}