package com.example.lodge.domain.profile

import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class profileUseCase @Inject constructor(private val userRepository: UserRepository) {


    suspend operator fun invoke(userId: String):UserModel{
        return userRepository.getUserById(userId = userId)
    }
}