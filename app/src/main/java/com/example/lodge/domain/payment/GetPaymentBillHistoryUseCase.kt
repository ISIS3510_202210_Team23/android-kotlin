package com.example.lodge.domain.payment

import com.example.lodge.data.cache.BillsReceiptsMemCache
import com.example.lodge.data.repository.PaymentRepository
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import com.example.lodge.domain.payment.model.BillView
import com.example.lodge.domain.payment.model.toView
import javax.inject.Inject

class GetPaymentBillHistoryUseCase @Inject constructor(
    private val paymentRepository: PaymentRepository,
    private val userRepository: UserRepository,
    private val memCache: BillsReceiptsMemCache
) {

    suspend operator fun invoke(userId: String, conn: Boolean, next:Boolean=false): List<BillView> {
        val billsMemCache = memCache.billsMemCache
        val pagedBills = memCache.completePagedBills
        val result: ArrayList<BillView> = ArrayList()
        val user: UserModel = userRepository.getUserById(userId = userId)

        if(conn){
            val idLast:String? = if (pagedBills.size>0) pagedBills[pagedBills.size-1].id else null
            val offset:String? = if (next) idLast else null

            val net = paymentRepository.getBillsNetwork(houseId = user.house, idOffset = offset)
            println()
            println(net)
            if(offset==null){
                pagedBills.clear()
                billsMemCache.clear()
                paymentRepository.clearBills()
                paymentRepository.insertBills(net)
                net.map {
                    billsMemCache.add(it.toView())
                }
                net.map {
                    pagedBills.add(it.toView())
                }
            } else{
                net.map {
                    pagedBills.add(it.toView())
                }
            }
            return pagedBills
        }
        else{
            pagedBills.clear()
            if(billsMemCache.size!=0){
                return billsMemCache
            }
            else{
                paymentRepository.getBills(complexId = user.complex)
                    .map {
                        result.add(it.toView())
                    }
            }
            return result
        }
    }
}