package com.example.lodge.data.repository.user

import android.net.Uri
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.user.UserModel

interface BaseUserClient {
    suspend fun getUserById(userId:String): UserModel
    suspend fun getHommies(userId: String,houseId: String): List<UserModel>
    suspend fun uploadUserPicture(id:String, image:Uri):Uri
}