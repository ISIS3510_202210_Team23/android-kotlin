package com.example.lodge.ui.viewmodel

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.cache.UserMemCache
import com.example.lodge.data.model.complex.Complex
import kotlinx.coroutines.channels.Channel
import com.example.lodge.data.repository.BaseAuthRepository
import com.example.lodge.data.repository.UserRepository
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repository : BaseAuthRepository,
    private val repoUser: UserRepository,
    private val userCache: UserMemCache,
) : ViewModel() {

    private val TAG = "RegisterLoginViewModel"
    val _firebaseUser = MutableLiveData<FirebaseUser?>()
    val currentUser get() = _firebaseUser

    //create our channels that will be used to pass messages to the main ui
    //create event channel
    private val eventsChannel = Channel<AllEvents>()
    private val eventsChannelCode = Channel<AllEvents>()
    private val eventsChannelComplex = Channel<AllEvents>()

    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()
    val allEventsFlowCode = eventsChannelCode.receiveAsFlow()
    val allEventsFlowComplex = eventsChannelComplex.receiveAsFlow()


    //validate all fields first before performing any sign in operations
    fun signInUser(email: String , password: String) = viewModelScope.launch{
        var pat = Patterns.EMAIL_ADDRESS

        when {
            email.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(1))
            }
            email.length >30 ->{
                eventsChannel.send(AllEvents.ErrorCode(3))
            }
            email.length <7 ->{
                eventsChannel.send(AllEvents.ErrorCode(4))
            }
            password.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(2))
            }
            password.length <6 -> {
                eventsChannel.send(AllEvents.ErrorCode(5))
            }
            password.length >30 -> {
                eventsChannel.send(AllEvents.ErrorCode(6))
            }
            !pat.matcher(email).matches() -> {
                eventsChannel.send(AllEvents.ErrorCode(7))
            }
            else -> {
                actualSignInUser(email , password)
            }
        }
    }

    //validate all fields before performing any sign up operations
    fun signUpUser(
        email: String,
        password: String,
        confirmPass: String,
        name: String,
        username: String,
        phone: String,
        gender: String,
        isAdmin: Boolean
    )= viewModelScope.launch {
       var pat = Patterns.EMAIL_ADDRESS
        var num = Patterns.PHONE
        if(gender.equals("Female") || gender.equals("Male")){
            eventsChannel.send(AllEvents.ErrorCode(18))
        }
        when{
            name.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(13))
            }
            username.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(14))
            }
            email.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(1))
            }
            phone.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(15))
            }
            gender.equals("-1") -> {
                eventsChannel.send((AllEvents.ErrorCode(16)))
            }
            password.isEmpty() -> {
                eventsChannel.send(AllEvents.ErrorCode(2))
            }
            password != confirmPass ->{
                eventsChannel.send(AllEvents.ErrorCode(3))
            }
            email.length >30 ->{
                eventsChannel.send(AllEvents.ErrorCode(4))
            }
            email.length <7 ->{
                eventsChannel.send(AllEvents.ErrorCode(5))
            }
            password.length <6 -> {
                eventsChannel.send(AllEvents.ErrorCode(17))
            }
            password.length >30 -> {
                eventsChannel.send(AllEvents.ErrorCode(6))
            }
            name.length <6 -> {
                eventsChannel.send(AllEvents.ErrorCode(9))
            }
            name.length >30 -> {
                eventsChannel.send(AllEvents.ErrorCode(8))
            }
            username.length <6 -> {
                eventsChannel.send(AllEvents.ErrorCode(11))
            }
            username.length >30 -> {
                eventsChannel.send(AllEvents.ErrorCode(10))
            }
            !pat.matcher(email).matches() -> {
                eventsChannel.send(AllEvents.ErrorCode(7))
            }
            !num.matcher(phone).matches() -> {
                eventsChannel.send(AllEvents.ErrorCode(12))
            }

            else -> {
                actualSignUpUser(email, password, name,username,phone,gender,isAdmin)
                eventsChannel.send(AllEvents.ErrorCode(100))
            }
        }
    }


    private fun actualSignInUser(email:String, password: String) = viewModelScope.launch {
       try {
            val user = repository.signInWithEmailPassword(email, password)
            user?.let {

                _firebaseUser.postValue(it)
                eventsChannel.send(AllEvents.Message("login success"))

            }
        }catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "signInUser: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }

    }

    private fun actualSignUpUser(
        email: String,
        password: String,
        name: String,
        username: String,
        phone: String,
        gender: String,
        isAdmin: Boolean
    ) = viewModelScope.launch {
        /*try {
            val user = repository.signUpWithEmailPassword(email, password)
            user?.let {
                repoUser.createUser(it.uid,email,name,username,phone,gender,isAdmin)
                _firebaseUser.postValue(it)
                eventsChannel.send(AllEvents.Message("sign up success"))

            }
        }catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "signInUser: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }*/
        userCache.name=name
        userCache.userName=username
        userCache.email=email
        userCache.gender=gender
        userCache.phone=phone
        userCache.password=password
        userCache.isAdmin=isAdmin
    }



    fun signOut() = viewModelScope.launch {
        try {
            val user = repository.signOut()
            user?.let {
                eventsChannel.send(AllEvents.Message("logout failure"))
            }?: eventsChannel.send(AllEvents.Message("sign out successful"))

            getCurrentUser()

        }catch(e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "signInUser: ${error[1]}")
            eventsChannel.send(AllEvents.Error(error[1]))
        }
    }

    fun associateComplex(code: String, id:String) = viewModelScope.launch {

        when{
            code.isEmpty() -> {
                eventsChannelCode.send(AllEvents.ErrorCode(1))
            }
            code.length!=8 -> {
                eventsChannelCode.send(AllEvents.ErrorCode(2))
            }

            else->{
                actualAssociateComplex(code,id)

            }
        }
    }


    private fun actualAssociateComplex(code:String, id:String) = viewModelScope.launch {

        try{

            val idComplex = repoUser.associateComplexVerification(code)
            println("VERIFICACION :"+idComplex)
            if(idComplex == true){
                val codeValue = code
                val user = repository.signUpWithEmailPassword(userCache.email, userCache.password)
                user?.let {
                    repoUser.associateComplex(code,user.uid)
                    _firebaseUser.postValue(it)
                    eventsChannel.send(AllEvents.Message("sign up success"))

                }
                //currentCode.postValue(codeValue)
                //eventsChannelCode.send(AllEvents.Message("association to complex success"))
            }
            else{
                eventsChannelCode.send(AllEvents.Error("Invalid code. Code doesn't exist"))
            }

        }
        catch (e: java.lang.Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "signInUser: ${error[1]}")
            eventsChannelCode.send(AllEvents.Error(error[1]))
        }


    }


    fun createComplex(name: String, city: String, address: String, id:String) = viewModelScope.launch {

        when{
            name.isEmpty() -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(1))
            }
            name.length>30 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(5))
            }
            name.length<6 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(4))
            }
            city.isEmpty()-> {
                eventsChannelComplex.send(AllEvents.ErrorCode(2))
            }
            city.length>30 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(7))
            }
            city.length<3 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(6))
            }
            address.isEmpty()->{
                eventsChannelComplex.send(AllEvents.ErrorCode(3))
            }
            address.length>30 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(9))
            }
            address.length<6 -> {
                eventsChannelComplex.send(AllEvents.ErrorCode(8))
            }
            else->{
                actualCreateComplex(name,city,address,id)

            }
        }
    }

    private fun actualCreateComplex(name:String,city:String,address:String, id:String) = viewModelScope.launch {

        try{
            val user = repository.signUpWithEmailPassword(userCache.email, userCache.password)
            user?.let {
                val houses = emptyList<String>()
                repoUser.createComplex(name,city,address,houses,it.uid)
                _firebaseUser.postValue(it)
                //val resicomplex = Complex(name,city,address,houses)
                //complex.postValue(resicomplex)
                eventsChannel.send(AllEvents.Message("sign up success"))
            }

        }
        catch (e: java.lang.Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "signInUser: ${error[1]}")
            eventsChannelComplex.send(AllEvents.Error(error[1]))
        }


    }

    fun getCurrentUser() = viewModelScope.launch {
        val user = repository.getCurrentUser()
        _firebaseUser.postValue(user)
    }


    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }

}