package com.example.lodge.domain.payment.model

import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.data.model.payment.db.Receipt
import com.google.firebase.Timestamp
import com.google.gson.GsonBuilder
import com.google.gson.ToNumberPolicy
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import kotlin.collections.HashMap

data class ReceiptView(var id: String="",
                       val bill: HashMap<String, Any> = HashMap<String, Any> (),
                       val date: Timestamp = Timestamp.now(),
                       val house:String="",
                       val paymentOption:HashMap<String, Any> = HashMap<String, Any> (),
                       val user:String="")



fun PaymentReceiptModel.toView() = ReceiptView(id = id, bill=bill, date=date, house=house, paymentOption = paymentOption, user = user)
fun Receipt.toView(): ReceiptView {
    return ReceiptView(id = id, bill = bill, date = Timestamp(date), house= house, paymentOption = paymentOption, user = user)
}
