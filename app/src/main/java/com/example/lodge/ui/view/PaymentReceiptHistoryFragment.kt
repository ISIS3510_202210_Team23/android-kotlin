package com.example.lodge.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lodge.databinding.FragmentPaymentHistoryListBinding
import com.example.lodge.ui.view.adapters.HistoryItemAdapter
import com.example.lodge.ui.view.adapters.HistoryItemListAdapter
import com.example.lodge.ui.view.adapters.HistoryItemMustPayListAdapter
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.ui.viewmodel.PaymentHistoryViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PaymentReceiptHistoryFragment: TrackedFragment() {

    override var trackingName: String = "PaymentReceiptHistoryFragment"

    private val paymentHistoryViewModel: PaymentHistoryViewModel by activityViewModels()
    private val registerViewModel : RegisterViewModel by activityViewModels()

    private var _binding: FragmentPaymentHistoryListBinding? = null
    private val binding get() = _binding!!

    private var columnCount = 1
    private var userId:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding= FragmentPaymentHistoryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvReceipts.adapter=HistoryItemListAdapter()
        binding.rvReceipts.layoutManager= when {
            columnCount <= 1 -> LinearLayoutManager(context)
            else -> GridLayoutManager(context, columnCount)
        }

        binding.idNestedSV.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
            // on scroll change we are checking when users scroll as bottom.
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                binding.idPBLoading.visibility=View.VISIBLE

                val conn = context?.let { it1 -> NetworkHelper.checkForInternet(it1) }
                if (conn != null) {
                    userId?.let { paymentHistoryViewModel.getNextReceipts(userId = it, conn=conn) }
                }
            }
        })

        registerViewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                userId = user.uid
                val conn = context?.let { it1 -> NetworkHelper.checkForInternet(it1) }
                if (conn != null) {
                    paymentHistoryViewModel.onCreateReceipts(userId = user.uid, conn=conn)
                }
            }
        }

        paymentHistoryViewModel.paymentReceiptHistoryModel.observe(viewLifecycleOwner) { history ->
            (binding.rvReceipts.adapter as HistoryItemListAdapter).submitList(history)
            binding.idPBLoading.post(Runnable { binding.idPBLoading.visibility=View.GONE })
        }
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"
        @JvmStatic
        fun newInstance(columnCount: Int) =
            PaymentReceiptHistoryFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}