package com.example.lodge.ui.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodge.R
import com.example.lodge.databinding.FragmentPaymentHistoryItemMustPayBinding
import com.example.lodge.domain.payment.model.BillView

class HistoryItemMustPayListAdapter(
    private val navController: NavController
    ):
    ListAdapter<BillView, HistoryItemMustPayListAdapter.MustPayViewHolder>(BillDiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MustPayViewHolder {
        return MustPayViewHolder(
            FragmentPaymentHistoryItemMustPayBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MustPayViewHolder, position: Int) {
        val item = getItem(position)
        holder.subjectView.text = item.name
        holder.amountView.text = item.amount.toString()
        holder.historyTimeView.text = item.dueDate.toDate().toString()
        holder.payButton.setOnClickListener{
            val bundle = bundleOf(
                "amount" to item.amount.toString(),
                "billid" to item.id
            )
            navController.navigate(R.id.action_paymentMainFragment_to_paymentGo2PayFragment, bundle)
        }
    }


    /**
     * Provide a reference to the views for each data item
    Complex data items may need more than one view per item, and
    you provide access to all the views for a data item in a view holder.
    Each data item is just an Affirmation object.
     */
    inner class MustPayViewHolder(binding: FragmentPaymentHistoryItemMustPayBinding) :
        RecyclerView.ViewHolder(binding.root)  {
        val subjectView: TextView = binding.historyItemSubject
        val amountView: TextView = binding.historyItemAmount
        val historyTimeView: TextView = binding.historyItemTime
        val payButton: Button = binding.payButton

        override fun toString(): String {
            return super.toString() + " '" + subjectView.text + "'"
        }
    }
}

object BillDiffCallback : DiffUtil.ItemCallback<BillView>() {
    override fun areItemsTheSame(oldItem: BillView, newItem: BillView): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: BillView, newItem: BillView): Boolean {
        return oldItem.id == newItem.id
    }
}