package com.example.lodge.dependencyInjection

import android.app.Application
import androidx.room.Room
import com.example.lodge.data.cache.BillsReceiptsMemCache
import com.example.lodge.data.cache.UserMemCache
import com.example.lodge.data.db.PaymentDB
import com.example.lodge.data.network.payment.PaymentApiClient
import com.example.lodge.data.repository.AuthRepository
import com.example.lodge.data.repository.BaseAuthRepository
import com.example.lodge.data.repository.firebase.BaseAuthenticator
import com.example.lodge.data.repository.firebase.FirebaseAuthenticator
import com.example.lodge.data.repository.user.BaseUserCreation
import com.example.lodge.data.repository.user.UserCreation
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/*
This module will provide complicated dependencies like external libraries
or dependencies of classes with interfaces

@InstallIn refers to the scope of the lifecycle for the intantiated classes.
For information https://dagger.dev/hilt/components.html

 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    // Provide Retrofit
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://lodge-40e24-default-rtdb.firebaseio.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    // Provide Retrofit Api Client here because is an interface
    @Singleton
    @Provides
    fun providePaymentApiClient(retrofit: Retrofit): PaymentApiClient {
        return retrofit.create(PaymentApiClient::class.java)
    }

    @Singleton
    @Provides
    fun provideAuthenticator() : BaseAuthenticator {
        return  FirebaseAuthenticator()
    }

    @Singleton
    @Provides
    fun provideCreation(db:FirebaseFirestore) : BaseUserCreation {
        return  UserCreation(db)
    }

    //this just takes the same idea as the authenticator. If we create another repository class
    //we can simply just swap here
    @Singleton
    @Provides
    fun provideRepository(
        authenticator : BaseAuthenticator
    ) : BaseAuthRepository {
        return AuthRepository(authenticator)
    }

    @Singleton
    @Provides
    fun provideMemCache():BillsReceiptsMemCache {
        return BillsReceiptsMemCache
    }
    @Singleton
    @Provides
    fun provideUserMemCache():UserMemCache {
        return UserMemCache
    }


    @Singleton
    @Provides
    fun provideFireStoreDB(): FirebaseFirestore{
        var db = FirebaseFirestore.getInstance()
        /*val settings = firestoreSettings {
            isPersistenceEnabled = false
        }
        db.firestoreSettings = settings*/
        return db
    }

    @Singleton
    @Provides
    fun provideFirebaseStorage(): FirebaseStorage {
        return FirebaseStorage.getInstance()
    }

    @Provides
    @Singleton
    fun provideDatabase(app: Application): PaymentDB =
        Room.databaseBuilder(app, PaymentDB::class.java, "payment_database")
            .build()


}