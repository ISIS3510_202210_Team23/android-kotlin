package com.example.lodge.domain.payment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.lodge.ui.view.MainActivity
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingEvent

class GeofenceReceiver : BroadcastReceiver() {
    lateinit var latitud: String
    lateinit var longitud: String

    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            val geofencingEvent = GeofencingEvent.fromIntent(intent)
            val geofencingTransition = geofencingEvent.geofenceTransition

            if (geofencingTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofencingTransition == Geofence.GEOFENCE_TRANSITION_DWELL) {
                // Retrieve data from intent
                if (intent != null) {
                    println(intent.extras)
                    //latitud = intent.getStringExtra("key")
                    //longitud = intent.getStringExtra("message")
                }

                MainActivity.showNotification(context.applicationContext,"Welcome to the jungle")

                // remove geofence
                val triggeringGeofences = geofencingEvent.triggeringGeofences
                MainActivity.removeGeofences(context, triggeringGeofences)
            }
        }
    }
}