package com.example.lodge.data.repository.finance

import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.db.Bill
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FinanceFirebase @Inject constructor(
    private val firestore: FirebaseFirestore
) {

    suspend fun financeEvent(){
        withContext(Dispatchers.IO){
            firestore.collection("Stats").document("statsFeature").update("countFinances", FieldValue.increment(1)).await()
        }
    }

    suspend fun getFinanceStats(complexId:String): Map<String, Int>{
        return withContext(Dispatchers.IO){
            var query:Query = firestore.collection("Bills")
                .whereEqualTo("complex", complexId)
                .orderBy("publishDate", Query.Direction.DESCENDING)

            var lastDocId=""
            var totalCount=0
            var paidCount=0

            while (true){
                var offsetQuery:Query = if (lastDocId!="")
                {
                    var lastDoc = firestore.collection("Bills").document(lastDocId).get().await()
                    query.startAfter(lastDoc)
                } else
                {
                    query
                }

                var snapshot =  offsetQuery.limit(200).get().await()

                for (doc in snapshot.documents){
                    var bill = doc.toObject(PaymentBillModel::class.java)

                    if (bill != null) {
                        if (bill.paid){
                            paidCount++
                        }
                    }
                }

                var size = snapshot.size()
                if (size==0) break
                totalCount+=size
                lastDocId = snapshot.documents[size-1].id
            }

            mapOf("Paid" to paidCount, "NotPaid" to totalCount-paidCount)
        }
    }


}