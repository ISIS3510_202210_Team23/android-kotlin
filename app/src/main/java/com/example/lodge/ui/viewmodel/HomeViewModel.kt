package com.example.lodge.ui.viewmodel


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.data.model.complex.Complex
import com.example.lodge.data.repository.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repoComplx: UserRepository
) : ViewModel() {

    private val TAG = "HomeViewModel"
    private val complex = MutableLiveData<ArrayList<Double>?>()
    val currentComplex get() = complex
    private val eventsChannel = Channel<HomeViewModel.AllEvents>()
    //the messages passed to the channel shall be received as a Flowable
    //in the ui
    val allEventsFlow = eventsChannel.receiveAsFlow()

    fun getComplexCoor(id: String)= viewModelScope.launch{
        try {
            val coords = repoComplx.getComplexCoor(id)
            if(coords!=null){
                currentComplex.postValue(coords)
                eventsChannel.send(HomeViewModel.AllEvents.Message("coords of complex success"))

            }
            else{
                eventsChannel.send(HomeViewModel.AllEvents.Error("Invalid code. Code doesn't exist"))
            }
        }
        catch (e:Exception){
            val error = e.toString().split(":").toTypedArray()
            Log.d(TAG, "register complex: ${error[1]}")
            eventsChannel.send(HomeViewModel.AllEvents.Error(error[1]))
        }

    }



    sealed class AllEvents {
        data class Message(val message : String) : AllEvents()
        data class ErrorCode(val code : Int):AllEvents()
        data class Error(val error : String) : AllEvents()
    }
}