package com.example.lodge.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun stringToMap(value: String): HashMap<String, String> {
        return Gson().fromJson(value,  object : TypeToken<HashMap<String, String>>() {}.type)
    }

    @TypeConverter
    fun mapToString(value: HashMap<String, String>?): String {
        return if(value == null) "" else Gson().toJson(value)
    }
    @TypeConverter
    fun stringToMapAny(value: String): HashMap<String, Any> {
        return Gson().fromJson(value,  object : TypeToken<HashMap<String, Any>>() {}.type)
    }

    @TypeConverter
    fun mapToStringAny(value: HashMap<String, Any>?): String {
        return if(value == null) "" else Gson().toJson(value)
    }
}