package com.example.lodge.data.repository.firebase

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class FirebaseAuthenticator: BaseAuthenticator {

    override suspend fun signUpWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(IO){
            Firebase.auth.createUserWithEmailAndPassword(email,password).await()

            Firebase.auth.currentUser
        }

    }

    override suspend fun signInWithEmailPassword(email: String, password: String): FirebaseUser? {
        return withContext(IO){
            Firebase.auth.signInWithEmailAndPassword(email , password).await()
            Firebase.auth.currentUser
        }

    }

    override fun signOut(): FirebaseUser? {
        Firebase.auth.signOut()
        return Firebase.auth.currentUser
    }

    override fun getUser(): FirebaseUser? {
        return Firebase.auth.currentUser
    }
}