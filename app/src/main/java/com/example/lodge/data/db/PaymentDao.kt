package com.example.lodge.data.db
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.data.model.payment.db.Bill
import com.example.lodge.data.model.payment.db.Receipt
import kotlinx.coroutines.flow.Flow

@Dao
interface PaymentDao {

    // Query to fetch all the data from the
    // SQLite database
    // No need of suspend method here



    @Query("SELECT * FROM receipts WHERE user=:userId AND house=:houseId")

    // Kotlin flow is an asynchronous stream of values
    suspend fun getAllReceipts(userId:String, houseId:String): List<Receipt>


    // If a new data is inserted with same primary key
    // It will get replaced by the previous one
    // This ensures that there is always a latest
    // data in the database


    @Insert(onConflict = OnConflictStrategy.REPLACE)

    // The fetching of data should NOT be done on the
    // Main thread. Hence coroutine is used
    // If it is executing on one one thread, it may suspend
    // its execution there, and resume in another one
    suspend fun insertReceipts(receipts: List<Receipt>)

    // Once the device comes online, the cached data
    // need to be replaced, i.e. delete it
    // Again it will use coroutine to achieve this task


    @Query("DELETE FROM receipts")
    suspend fun deleteAllReceipts()
}