package com.example.lodge.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.lodge.data.model.payment.db.Bill
import kotlinx.coroutines.flow.Flow

@Dao
interface BillsDao {

    @Query("SELECT * FROM bills WHERE complex=:complexId")

    // Kotlin flow is an asynchronous stream of values
    suspend fun getAllBills(complexId:String): List<Bill>

    @Insert(onConflict = OnConflictStrategy.REPLACE)

    // The fetching of data should NOT be done on the
    // Main thread. Hence coroutine is used
    // If it is executing on one one thread, it may suspend
    // its execution there, and resume in another one
    suspend fun insertBills(bills: List<Bill>)

    @Query("DELETE FROM bills")
    suspend fun deleteAllBills()
}