package com.example.lodge.data.model.payment

import androidx.annotation.DrawableRes
import com.google.gson.annotations.SerializedName

/**
 * Data model for the payment history item
 */
data class PaymentHistoryModel(
    @SerializedName("amount") val amount:Double,
    @SerializedName("paymentDateTime") val paymentDateTime:String,
    @SerializedName("paymentStatus") val paymentStatus:String,
    @SerializedName("subject") val subject:String
    )
