package com.example.lodge.ui.view.trackers

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.lodge.ui.viewmodel.FinanceViewModel
import com.example.lodge.ui.viewmodel.TrackingViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.*
import java.time.ZoneId.of
import java.time.format.DateTimeFormatter
import java.util.*


open class TrackedFragment: Fragment() {

    /**
     * This describes the name of the fragment being tracked. Must be overriden because its going to be used for DB data
     */
    protected open lateinit var trackingName:String

    /**
     * Datetime for the starttime registered when the onStart lifecycle method is executed.
     */
    private lateinit var startDateTimeHour:String

    /**
     * Start time when the registered when the onStart lifecycle method is executed.
     */
    private var startTime: Long? = null

    /**
     * End time when the registered when the onStop lifecycle method is executed.
     */
    private var endTime: Long? = null

    private val trackingViewModel: TrackingViewModel by activityViewModels()

    override fun onStart() {
        super.onStart()
        val rightNow = Calendar.getInstance()
        val currentHourIn24Format: Int =rightNow.get(Calendar.HOUR_OF_DAY)
        startDateTimeHour=currentHourIn24Format.toString()
        startTime=System.currentTimeMillis()
    }

    override fun onStop() {
        super.onStop()
        endTime=System.currentTimeMillis()
        var duration = endTime!! - startTime!!
        trackingViewModel.registerPickHours(trackingName, startDateTimeHour, duration)
    }

}