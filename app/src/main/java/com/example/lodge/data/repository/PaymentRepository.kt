package com.example.lodge.data.repository

import com.example.lodge.data.db.PaymentDB
import com.example.lodge.data.model.payment.*
import com.example.lodge.data.model.payment.db.Bill
import com.example.lodge.data.model.payment.db.Receipt
import com.example.lodge.data.model.payment.db.toDatabase
import com.example.lodge.data.network.payment.PaymentService
import com.example.lodge.data.repository.payment.PaymentFirebaseClient
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PaymentRepository @Inject constructor(
    private val paymentClient: PaymentFirebaseClient,
    private val service:PaymentService,
    private val paymentProvider: PaymentProvider,
    private val db: PaymentDB
) {
    private val paymentDao = db.paymentDao()
    private val billsDao = db.billsDao()

    suspend fun getBillById(billId:String): PaymentBillModel = paymentClient.getBillById(billId)

    suspend fun createReceiptPaidBill(amount:String, userId: String, bill:PaymentBillModel, houseId: String):PaymentReceiptModel=paymentClient.createReceiptPaidBill(amount=amount, userId = userId, bill = bill, houseId = houseId)

    suspend fun getPaymentGeneralInfo(): PaymentGeneralModel {
        val response: PaymentGeneralModel? = service.getPaymentGeneralInfo()
        paymentProvider.paymentGeneralInfo = response!!
        return response
    }

    suspend fun getBillsNetwork(houseId: String, idOffset:String?, limit:Int=20): List<Bill> {
        val apiRes = paymentClient.getBillsByHouse(houseId = houseId, idOffset, limit)
        return apiRes.map { it.toDatabase() }

    }
    suspend fun getBills(complexId:String): List<Bill> {
        return withContext(IO){
            val data = billsDao.getAllBills(complexId)
            data
        }
    }

    suspend fun insertBills(bills:List<Bill>){
        withContext(IO){
            billsDao.insertBills(bills)
        }
    }

    suspend fun clearBills(){
        withContext(IO){
            billsDao.deleteAllBills()
        }
    }

    suspend fun getReceipts(userId:String, houseId:String): List<Receipt> {
        return withContext(IO){
            val data = paymentDao.getAllReceipts(userId,houseId)
            data
        }
    }
    suspend fun getReceiptsNetwork(userId:String, houseId:String, idOffset:String?, limit:Int=20): List<Receipt> {
        val apiRes = paymentClient.getReceiptsPaidByUser(userId, houseId, idOffset, limit)
        return apiRes.map { it.toDatabase() }

    }

    suspend fun insertReceipts(receipts:List<Receipt>){
        withContext(IO){
            paymentDao.insertReceipts(receipts)
        }

    }
    suspend fun clearReceipts(){
        withContext(IO){
            paymentDao.deleteAllReceipts()
        }
    }
}




