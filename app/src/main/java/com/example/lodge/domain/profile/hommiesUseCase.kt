package com.example.lodge.domain.profile

import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class hommiesUseCase @Inject constructor(private val userRepository: UserRepository) {

    suspend operator fun invoke(userId:String,houseId:String): List<UserModel>{
        return userRepository.getHommies(userId,houseId)
    }


}