package com.example.lodge.data.repository.tracking

interface BaseTracking {

    suspend fun registerPeakHourTrack(componentName:String, startTime:String, duration:Long)
}