package com.example.lodge.domain.payment

import com.example.lodge.data.repository.PaymentRepository
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentReceiptModel
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class CreateReceiptPaidBillUseCase @Inject constructor(
    private val paymentRepository: PaymentRepository,
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(userId:String, amount: Double, billId:String): PaymentReceiptModel {
        var result:PaymentReceiptModel=PaymentReceiptModel()
        val user:UserModel=userRepository.getUserById(userId = userId)
        user?.let { userModel ->
            val billResult:PaymentBillModel=paymentRepository.getBillById(billId)
            billResult?.let { bill ->
                bill.id=billId
                result=paymentRepository.createReceiptPaidBill(userId = user.id, amount = amount.toString(), houseId = user.house, bill = bill)
            }
        }
        return result
    }

}