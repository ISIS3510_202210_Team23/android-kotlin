package com.example.lodge.ui.view

import android.annotation.SuppressLint
import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts.StartIntentSenderForResult
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.lodge.R
import com.example.lodge.databinding.FragmentPaymentGo2payBinding
import com.example.lodge.ui.view.trackers.TrackedFragment
import com.example.lodge.ui.viewmodel.PaymentGeneralViewModel
import com.example.lodge.ui.viewmodel.PaymentHistoryViewModel
import com.example.lodge.ui.viewmodel.RegisterViewModel
import com.example.lodge.utils.NetworkHelper
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.wallet.PaymentData
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import javax.inject.Inject

@AndroidEntryPoint
class PaymentGo2PayFragment @Inject constructor(): TrackedFragment() {

    override var trackingName: String = "PaymentGo2PayFragment"

    private val paymentGeneralViewModel: PaymentGeneralViewModel by activityViewModels()
    private val paymentHistoryViewModel: PaymentHistoryViewModel by activityViewModels()
    private val registerViewModel: RegisterViewModel by activityViewModels()

    private lateinit var googlePayButton: View

    private var _binding: FragmentPaymentGo2payBinding? = null
    private val binding get() = _binding!!

    private lateinit var amountText: String
    private lateinit var billId: String
    private lateinit var userId: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            amountText= it.getString("amount")!!
            billId=it.getString("billid")!!
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate
        _binding = FragmentPaymentGo2payBinding.inflate(layoutInflater)
        googlePayButton = binding.googlePayButton.root
        googlePayButton.setOnClickListener {
            activity?.let { it1 -> if(NetworkHelper.checkForInternet(it1, view, inflater)){
                requestPayment()
            }
            }
        }

        var viewTopBar: View? = activity?.findViewById(R.id.appBarLayout)
        var viewBotBar: View? = activity?.findViewById(R.id.navigation)
        if (viewTopBar != null) {
            viewTopBar.visibility=View.VISIBLE
        }
        if (viewBotBar != null) {
            viewBotBar.visibility=View.VISIBLE
        }

        registerViewModel.currentUser.observe(viewLifecycleOwner) { user ->
            user?.let {
                userId=user.uid
            }
        }

        binding.amountView.text= formatBalance(amountText.toDouble())

        // Check whether Google Pay can be used to complete a payment
        paymentGeneralViewModel.canUseGooglePay.observe(viewLifecycleOwner, Observer(::setGooglePayAvailable))

        return binding.root
    }

    private fun formatBalance(balance:Double=0.0):String{
        val format: NumberFormat = NumberFormat.getCurrencyInstance()
        format.maximumFractionDigits=2
        //format.currency = Currency.getInstance("EUR")
        return format.format(balance)
    }

    // Handle potential conflict from calling loadPaymentData.
    private val resolvePaymentForResult = registerForActivityResult(StartIntentSenderForResult()) {
            result: ActivityResult ->
        when (result.resultCode) {
            RESULT_OK ->
                result.data?.let { intent ->
                    PaymentData.getFromIntent(intent)?.let(::handlePaymentSuccess)
                }

            RESULT_CANCELED -> {
                // The user cancelled the payment attempt
            }
        }
    }

    /**
     * If isReadyToPay returned `true`, show the button and hide the "checking" text. Otherwise,
     * notify the user that Google Pay is not available. Please adjust to fit in with your current
     * user flow. You are not required to explicitly let the user know if isReadyToPay returns `false`.
     *
     * @param available isReadyToPay API response.
     */
    private fun setGooglePayAvailable(available: Boolean) {
        if (available) {
            googlePayButton.visibility = View.VISIBLE
        } else {
            Toast.makeText(
                context,
                R.string.googlepay_status_unavailable,
                Toast.LENGTH_LONG).show()
        }
    }

    private fun requestPayment() {

        // Disables the button to prevent multiple clicks.
        googlePayButton.isClickable = false

        // The price provided to the API should include taxes and shipping.
        // This price is not displayed to the user.
        val dummyPriceCents = 100L
        val shippingCostCents = 900L
        val task = paymentGeneralViewModel.getLoadPaymentDataTask(dummyPriceCents + shippingCostCents)

        task.addOnCompleteListener { completedTask ->
            if (completedTask.isSuccessful) {
                print("PAGO EXITOSO")
                completedTask.result.let(::handlePaymentSuccess)
            } else {
                when (val exception = completedTask.exception) {
                    is ResolvableApiException -> {
                        resolvePaymentForResult.launch(
                            IntentSenderRequest.Builder(exception.resolution).build())
                    }
                    is ApiException -> {
                        handleError(exception.statusCode, exception.message)
                    }
                    else -> {
                        handleError(
                            CommonStatusCodes.INTERNAL_ERROR, "Unexpected non API" +
                                " exception when trying to deliver the task result to an activity!")
                    }
                }
            }

            // Re-enables the Google Pay payment button.
            googlePayButton.isClickable = true
        }
    }

    /**
     * PaymentData response object contains the payment information, as well as any additional
     * requested information, such as billing and shipping address.
     *
     * @param paymentData A response object returned by Google after a payer approves payment.
     * @see [Payment
     * Data](https://developers.google.com/pay/api/android/reference/object.PaymentData)
     */
    @SuppressLint("StringFormatInvalid")
    private fun handlePaymentSuccess(paymentData: PaymentData) {
        val paymentInformation = paymentData.toJson()

        try {
            // Token will be null if PaymentDataRequest was not constructed using fromJson(String).
            val paymentMethodData = JSONObject(paymentInformation).getJSONObject("paymentMethodData")
            val billingName = paymentMethodData.getJSONObject("info")
                .getJSONObject("billingAddress").getString("name")
            Log.d("BillingName", billingName)

            Toast.makeText(context, getString(R.string.payments_show_name, billingName), Toast.LENGTH_LONG).show()

            paymentGeneralViewModel.createReceiptSuccessfulPayment(userId = userId, amount = amountText.toDouble(), billId = billId )
            //Refresh lists
            paymentHistoryViewModel.onCreateBills(userId, true)
            paymentHistoryViewModel.onCreateReceipts(userId, true)

            // Logging token string.
            Log.d("Google Pay token", paymentMethodData
                .getJSONObject("tokenizationData")
                .getString("token"))

        } catch (error: JSONException) {
            Log.e("handlePaymentSuccess", "Error: $error")
        }

    }

    /**
     * At this stage, the user has already seen a popup informing them an error occurred. Normally,
     * only logging is required.
     *
     * @param statusCode will hold the value of any constant from CommonStatusCode or one of the
     * WalletConstants.ERROR_CODE_* constants.
     * @see [
     * Wallet Constants Library](https://developers.google.com/android/reference/com/google/android/gms/wallet/WalletConstants.constant-summary)
     */
    private fun handleError(statusCode: Int, message: String?) {
        Log.w("loadPaymentData failed", "Error code: $statusCode, Message: $message")
    }


    companion object {
        const val ARG_COLUMN_COUNT = "column-count"
        @JvmStatic
        fun newInstance(columnCount: Int) =
            PaymentGo2PayFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}