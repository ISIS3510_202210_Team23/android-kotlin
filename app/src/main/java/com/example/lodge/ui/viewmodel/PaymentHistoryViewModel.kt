package com.example.lodge.ui.viewmodel

import android.text.BoringLayout
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.lodge.domain.payment.GetPaymentBillHistoryUseCase
import com.example.lodge.domain.payment.GetPaymentReceiptHistoryUseCase
import com.example.lodge.domain.payment.model.BillView
import com.example.lodge.domain.payment.model.ReceiptView
import com.example.lodge.utils.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Integer.max
import javax.inject.Inject
import kotlin.math.min

@HiltViewModel
class PaymentHistoryViewModel @Inject constructor(
    private val getPaymentBillHistoryUseCase: GetPaymentBillHistoryUseCase,
    private val getPaymentReceiptHistoryUseCase: GetPaymentReceiptHistoryUseCase,
): ViewModel() {

    val paymentBillHistoryModel = MutableLiveData<List<BillView>>()
    val paymentReceiptHistoryModel = MutableLiveData<List<ReceiptView>>()

    fun onCreateBills(userId:String, conn:Boolean){
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val bills = getPaymentBillHistoryUseCase(userId = userId, conn = conn)
                paymentBillHistoryModel.postValue(ArrayList(bills.map { it.copy() }))
            }
        }
    }

    fun onCreateReceipts(userId: String, conn:Boolean){
        viewModelScope.launch {

            withContext(Dispatchers.Default) {
                val receipts = getPaymentReceiptHistoryUseCase(userId = userId, conn = conn)
                paymentReceiptHistoryModel.postValue(ArrayList(receipts.map { it.copy() }))
            }
        }
    }

    fun getNextBills(userId: String, conn:Boolean){
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val bills = getPaymentBillHistoryUseCase(userId = userId, conn = conn, next = true)
                paymentBillHistoryModel.postValue(ArrayList(bills.map { it.copy() }))
            }
        }
    }

    fun getNextReceipts(userId: String, conn:Boolean){
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                val receipts =
                    getPaymentReceiptHistoryUseCase(userId = userId, conn = conn, next = true)
                paymentReceiptHistoryModel.postValue(ArrayList(receipts.map { it.copy() }))
            }
        }
    }

}