package com.example.lodge.ui.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.lodge.databinding.FragmentPaymentHistoryItemBinding
import com.example.lodge.domain.payment.model.ReceiptView

class HistoryItemListAdapter: ListAdapter<ReceiptView, HistoryItemListAdapter.ViewHolder>(ReceiptDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentPaymentHistoryItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        if(item.bill["name"] !=null){
            holder.subjectView.text = item.bill["name"] as String
        }
        holder.amountView.text = item.house
        holder.historyTimeView.text = item.date.toDate().toString()
    }

    /**
     * Provide a reference to the views for each data item
    Complex data items may need more than one view per item, and
    you provide access to all the views for a data item in a view holder.
    Each data item is just an Affirmation object.
     */
    inner class ViewHolder(binding: FragmentPaymentHistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val subjectView: TextView = binding.historyItemSubject
        val amountView: TextView = binding.historyItemAmount
        val historyTimeView: TextView = binding.historyItemTime

        override fun toString(): String {
            return super.toString() + " '" + subjectView.text + "'"
        }
    }
}

object ReceiptDiffCallback : DiffUtil.ItemCallback<ReceiptView>() {
    override fun areItemsTheSame(oldItem: ReceiptView, newItem: ReceiptView): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ReceiptView, newItem: ReceiptView): Boolean {
        return oldItem.id == newItem.id
    }
}
