package com.example.lodge.domain.complex

import com.example.lodge.data.model.user.AdminModel
import com.example.lodge.data.model.user.UserModel
import com.example.lodge.data.repository.ComplexRepository
import com.example.lodge.data.repository.UserRepository
import javax.inject.Inject

class GetComplexAdministrators @Inject constructor(
    private val complexRepository: ComplexRepository,
    private val userRepository: UserRepository
) {
    suspend operator fun invoke(userId:String): List<AdminModel> {
        val user: UserModel =userRepository.getUserById(userId = userId)
        return complexRepository.getComplexAdministrators(user.complex)
    }
}