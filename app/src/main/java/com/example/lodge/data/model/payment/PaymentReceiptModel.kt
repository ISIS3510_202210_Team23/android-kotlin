package com.example.lodge.data.model.payment

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.firebase.Timestamp
import java.util.*
import kotlin.collections.HashMap


data class PaymentReceiptModel(
    var id: String="",
    val bill: HashMap<String, Any> = HashMap<String, Any> (),
    val date: Timestamp=Timestamp.now(),
    val house:String="",
    val paymentOption: HashMap<String, Any> = HashMap<String, Any> (),
    val user:String=""
)
