package com.example.lodge.data.repository

import com.example.lodge.data.repository.finance.FinanceFirebase
import javax.inject.Inject

class FinanceRepository @Inject constructor(
    private val financeFirebase: FinanceFirebase
) {
    suspend fun financeEvent() = financeFirebase.financeEvent()

    suspend fun getFinanceStats(complexId:String) = financeFirebase.getFinanceStats(complexId = complexId)

}