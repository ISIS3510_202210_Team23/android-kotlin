package com.example.lodge.ui.view.adapters

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.contentValuesOf
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import com.example.lodge.R
import com.example.lodge.data.model.payment.PaymentBillModel
import com.example.lodge.data.model.payment.PaymentHistoryModel
import com.example.lodge.databinding.FragmentPaymentHistoryItemBinding
import com.example.lodge.databinding.FragmentPaymentHistoryItemMustPayBinding
import com.example.lodge.domain.payment.model.BillView

import com.example.lodge.ui.view.placeholder.PlaceholderContent.PlaceholderItem

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class HistoryItemMustPayAdapter(
    private var values: List<BillView>,
    private val navController: NavController
) : RecyclerView.Adapter<HistoryItemMustPayAdapter.MustPayViewHolder>() {

    lateinit var binding: FragmentPaymentHistoryItemMustPayBinding


    fun updateData(newSet:List<BillView>){
        values=newSet
        notifyDataSetChanged()
    }

    /**
     * Create new views (invoked by the layout manager)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MustPayViewHolder {
        println()
        println("ON CREATE VIEW HOLDER")
        return MustPayViewHolder(
            FragmentPaymentHistoryItemMustPayBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    /**
     * Replace the contents of a view (invoked by the layout manager)
     */
    override fun onBindViewHolder(holder: MustPayViewHolder, position: Int) {
        println()
        println("ON BIND VIEW HOLDER")
        val item = values[position]
        holder.subjectView.text = item.name
        holder.amountView.text = item.amount.toString()
        holder.historyTimeView.text = item.dueDate.toDate().toString()
        holder.payButton.setOnClickListener{
            val bundle = bundleOf(
                "amount" to item.amount.toString(),
                "billid" to item.id
            )
            navController.navigate(R.id.action_paymentMainFragment_to_paymentGo2PayFragment, bundle)
        }

    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     */
    override fun getItemCount(): Int = values.size

    /**
     * Provide a reference to the views for each data item
        Complex data items may need more than one view per item, and
        you provide access to all the views for a data item in a view holder.
        Each data item is just an Affirmation object.
      */
    inner class MustPayViewHolder(binding: FragmentPaymentHistoryItemMustPayBinding) :
        RecyclerView.ViewHolder(binding.root)  {
        val subjectView: TextView = binding.historyItemSubject
        val amountView: TextView = binding.historyItemAmount
        val historyTimeView: TextView = binding.historyItemTime
        val payButton:Button = binding.payButton

        override fun toString(): String {
            return super.toString() + " '" + subjectView.text + "'"
        }
    }

}